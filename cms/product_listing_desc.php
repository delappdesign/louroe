<? 


require_once '../include/data_inc.php';


 $ID = $_GET['ID']; if ($ID=='') $ID=1;

 $sql_product = "select p.*, c.CategoryID, c.CategoryName, c.CategoryFolder from products p 
 left join categories c on p.CategoryID = c.CategoryID
 order by c.CategoryID, p.ProductName ";
 $mysqli_result_product = mysqli_query($mysqli, "$sql_product"); if (!$mysqli_result_product) { echo 'error in product'; exit; }



$export = true;
if ( $export )
{

	$dt = new DateTime();
	$theDate = date_format($dt, 'Y-m-d');

	$filename ="product_descriptions.xls";
	
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
}
?>


<style>
	td {font-family: "Trebuchet MS"; font-size: 10pt;vertical-align: top;}
	br { mso-data-placement:same-cell; }
</style>


<body>
<table border=1>

	
<?
$currCategoryID = '';
while($row_product = mysqli_fetch_assoc($mysqli_result_product))
{
	foreach ($row_product as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
	if ($currCategoryID<>$CategoryID)
	{
		$currCategoryID = $CategoryID;

		
?>
	<tr>
		<td colspan=5><b><?=$CategoryName?></b><br><br></td>
	</tr>
	<tr>
		<td>ProductID</td>
		<td>Active</td>
		<td>ProductNumber</td>
		<td>ProductName</td>
		<td>Short Description</td>
		<td>Description</td>
	</tr>
<?
	}

		$S_image = "../products/images/{$CategoryFolder}/{$ProductNumber}_S_louroe.jpg";
		$L_image = "../products/images/{$CategoryFolder}/{$ProductNumber}_L_louroe.jpg";
		$A1_image = "../products/images/{$CategoryFolder}/{$ProductNumber}_A1_louroe.jpg";
		$A2_image = "../products/images/{$CategoryFolder}/{$ProductNumber}_A2_louroe.jpg";
		$A3_image = "../products/images/{$CategoryFolder}/{$ProductNumber}_A3_louroe.jpg";

		$Description = $Description_Web;
		
		$nl = '|||||';
		
		
		$Description = str_replace('</p>',$nl,$Description);
		$Description = str_replace('<p>',$nl,$Description);
		$Description = str_replace('<br>',$nl,$Description);
		$Description = str_replace('<br/>',$nl,$Description);
		$Description = str_replace('<br />',$nl,$Description);
		$Description = str_replace('<li>',$nl,$Description);
		
		$Description = strip_tags($Description, $nl);
		
		//$Description = str_replace('<br>',"<br style='mso-data-placement:same-cell;' />",$Description);
		

		//if ($Description!='') { echo $Description; exit; } 
		if ($Description=='') { $Description = '&nbsp;'; }
		
		if ($Active=='') { $Active = 'N'; }
		
		

?>
	
	<tr>
		<td><?=$ProductID?></td>
		<td><?=$Active?></td>
		<td><?=$ProductNumber?></td>
		<td><?=$ProductName?></td>
		<td><?=$Description_Short?></td>
		<td><?=$Description?></td>
	</tr>

<? 
	}
?>
</table>
</body>
