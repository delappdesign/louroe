<? require_once 'admin_header.php'; ?>
<style>
body { font-family: "Trebuchet MS"; font-size: 8pt; line-height: 12pt;}
a { text-decoration: none; }

</style>

<b>Menu
<br><br><b>General</b>
<br><a href='modify_categories.php'>Modify Category Descriptions</a>
<br><a href='modify_content.php'>Page Content</a>
<br><a href='modify_distributors.php'>Modify Distributors</a>
<br><a href='modify_applications.php'>Modify Applications</a>
<br><a href='modify_videos.php'>Modify Videos</a>
<br><a href='modify_manufacturers.php'>Modify Manufacturers</a>
<br><br><b>Products</b>
<br><a href='modify_products.php'>Modify Products</a>
<br><a href='modify_products.php?listing=inactive'>Modify Inactive Products</a>
<br><a href='product_listing.php'>Product Listing</a>
<br><br><b>News and Accolades</b>
<br><a href='modify_articles.php'>Modify Articles</a>
<br><a href='modify_accolades.php'>Modify Accolades</a>
<br><br><b>Forum</b>
<br><a href='modify_topics.php'>Modify Topics</a>
<br><a href='modify_posts.php'>Modify Posts</a>
<br><br><b>Events</b>
<br><a href='modify_events.php'>Modify Events</a>
<br><br><b>Users</b>
<br><a href='export_customers.php'>View Customers</a>&nbsp;&nbsp;<a href='export_customers.php?export'>(export)</a>
<br><a href='export_newsletter.php'>View Newsletter</a>&nbsp;&nbsp;<a href='export_newsletter.php?export'>(export)</a>
<br><br><b>Stats</b>
<br><a href='export_search.php'>View Searches</a>&nbsp;&nbsp;<a href='export_search.php?export'>(export)</a>
