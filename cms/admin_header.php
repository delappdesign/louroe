<? require_once '../include/data_inc.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Louroe</title>


<style>
body {
 margin: 0;
 padding: 0;
 color: #4f4f4f;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 13pt;
 font-style: normal;
 line-height: 18pt;
 font-weight: normal;
 font-variant: normal;
 text-transform: none;
 text-decoration: none;
 background-color: #fff;
}


.container {
 position: relative;
 width: 1180px;
 margin: 0 auto;
}

.container .header {
 width: 1180px;
 height: 50px;
 margin: 0;
 padding: 0;
 background: url(../img/global/body-header-repeat-bg.jpg) repeat-x;
}

.container .header .logoImg {
 float: left;
 width: 102px;
 height: 35px;
 margin: 2px 0 0 28px;
 
}

.headertext {
 width: 208px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 11px;
 font-style: normal;
 text-transform: none;
 color: #ffffff;
 margin: 0px 0 0px 140px;
 text-decoration: none;
 
}

</style>


<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>

</head>

<body>
<div class="container">
<div class="header"><a href="index.php" ><img src="../img/global/louroe-electronics-logo.png" width="102" height="35" class="logoImg" border="0"/></a>
<div class="headertext">CMS</div>
</div>
<br>