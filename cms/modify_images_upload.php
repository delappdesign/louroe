<?php

require_once '../include/data_inc.php';

$get_variables="autoid,id,type,action,image";
$get_array = explode(",",$get_variables); foreach ($get_array as $value) 
{ $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }

//echo '<pre>' . print_r($_GET, true) . '</pre><br>'; 
if ($id=='') { $id=1; }
if ($type=='') { $type='event'; }

function writeFilenameToDatabase ( $fileName )
{


	global $mysqli, $fh;
	global $id, $type;
	
	$image = $fileName;

	fwrite($fh, "\nwriteFilenameToDatabase - $type");

	if ($type=='event')
	{
	
		$sql = "insert into events_slides ( EventID, SlideImage ) values ( '$id', '$image' )";
		fwrite($fh, "\n$sql");
		$mysqli_result = mysqli_query($mysqli, "$sql"); if (!$mysqli_result) { echo 'error in sql'; exit; }
	}

}

/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

// HTTP headers for no cache etc
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$myFile = "{$INTERNAL_DIRECTORY}/events/_upload_log.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
$today = date("F j, Y, g:i:s a");
fwrite($fh, "\n------------------------");
fwrite($fh, "\n$today:\n\n");


// Settings
//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
//$targetDir = 'uploads/';

if ($type=='event') { $targetDir = "{$INTERNAL_DIRECTORY}/events/{$id}"; }
fwrite($fh, "\n$targetDir");


//$cleanupTargetDir = false; // Remove old files
//$maxFileAge = 60 * 60; // Temp file age in seconds

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Get parameters
$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

// Clean the fileName for security reasons
$fileName = preg_replace('/[^\w\._]+/', '', $fileName);

// Make sure the fileName is unique but only if chunking is disabled

/*
if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
	$ext = strrpos($fileName, '.');
	$fileName_a = substr($fileName, 0, $ext);
	$fileName_b = substr($fileName, $ext);

	$count = 1;
	while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
		$count++;

	$fileName = $fileName_a . '_' . $count . $fileName_b;
}
*/

// Create target dir
if (!file_exists($targetDir))
	@mkdir($targetDir);

// Remove old temp files
/* this doesn't really work by now
	
if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
	while (($file = readdir($dir)) !== false) {
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $file;

		// Remove temp files if they are older than the max age
		if (preg_match('/\\.tmp$/', $file) && (filemtime($filePath) < time() - $maxFileAge))
			@unlink($filePath);
	}

	closedir($dir);
} else
	die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
*/

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

if (isset($_SERVER["CONTENT_TYPE"]))
	$contentType = $_SERVER["CONTENT_TYPE"];

//fwrite($fh, "\n$contentType");

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false) 
{
	fwrite($fh, "\nmultipart 1a - ");
	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) 
	{
		// Open temp file
		fwrite($fh, "\nmultipart 1b - " . $targetDir . DIRECTORY_SEPARATOR . $fileName);
		$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
		if ($out) 
		{
			// Read binary input stream and append it to temp file
			$in = fopen($_FILES['file']['tmp_name'], "rb");
			if ($in) 
			{
				while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
				writeFilenameToDatabase ( $fileName );
				fwrite($fh, "\nmultipart 1c - UPLOAD SUCCESSFUL:" . $fileName ); 
			} 
			else
			{
				fwrite($fh, "\nmultipart 1d - error cannot open input"); fclose($fh);
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
			
			fclose($in);
			fclose($out);
			@unlink($_FILES['file']['tmp_name']);
		} 
		else 
		{
			fwrite($fh, "\nmultipart 1e - error cannot open output"); fclose($fh);
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}'); 
		}
	} 
	else
	{
		fwrite($fh, "\nmultipart 1f - error cannot move upload"); fclose($fh);
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
	}
} 
else 
{
	// Open temp file
	fwrite($fh, "\npart 1a - ");
	$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
	fwrite($fh, "\npart 1b - " . $targetDir . DIRECTORY_SEPARATOR . $fileName);
	if ($out) {
		// Read binary input stream and append it to temp file
		$in = fopen("php://input", "rb");

		if ($in) 
		{
			while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
			writeFilenameToDatabase ( $fileName );
			fwrite($fh, "\npart 1b - UPLOAD SUCCESSFUL:" . $fileName ); 
		} 
		else
		{	
			fwrite($fh, "\npart 1c - error cannot open input"); fclose($fh);
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		}

		fclose($in);
		fclose($out);
	} 
	else
	{
		fwrite($fh, "\npart 1d - error cannot open output"); fclose($fh);
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
	}
}

fwrite($fh, "\n------------------------\n\n");
fclose($fh);

// Return JSON-RPC response
echo('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');



?>