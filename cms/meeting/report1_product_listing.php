<? 

require_once '../../include/data_inc.php';


 $ID = $_GET['ID']; if ($ID=='') $ID=1;

 $sql_product = "select p.*, c.CategoryID, c.CategoryName, c.CategoryFolder from products p 
 left join categories c on p.CategoryID = c.CategoryID
 order by c.CategoryID, p.ProductName";
 $mysqli_result_product = mysqli_query($mysqli, "$sql_product"); if (!$mysqli_result_product) { echo 'error in product'; exit; }


$export = true;
if ( $export )
{

	$dt = new DateTime();
	$theDate = date_format($dt, 'Y-m-d');

	$filename ="export_products_{$theDate}.xls";
	
	//header('Content-type: application/ms-excel');
	//header('Content-Disposition: attachment; filename='.$filename);
}
?>
<style>
	td {font-family: "Arial"; font-size: 8pt; border:1px solid #aaa; height:50px; }
	table{empty-cells: show; border-collapse:collapse; }

	td.red { background-color: #ffbcbd }
	td.green { background-color: #b8f3d9; }
	.header>td { background-color: #ffeaa6; }
	.inactive>td { background-color: #ddd; }
</style>

<table>

<?

$currCategoryID = '';
$i = 0;
while($row_product = mysqli_fetch_assoc($mysqli_result_product))
{
	foreach ($row_product as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
	if ($currCategoryID<>$CategoryID)
	{
		$currCategoryID = $CategoryID;

		
?>
	<tr>
		<td colspan=11><b><?=$CategoryName?></b><br><br></td>
	</tr>
	<tr class='header'>
		<td>ProductID</td>
		<td>Active</td>
		<td>ProductNumber</td>
		<td>ProductSerial</td>
		<td>ProductName</td>
		<td style='min-width:25px;'>Spec</td>
		<td style='min-width:25px;'>Instr</td>
		<td style='min-width:25px;'>CAD</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td style='min-width:75px;'>L image</td>
		<td style='min-width:75px;'>A1 image</td>
		<td style='min-width:75px;'>A2 image</td>
		<td style='min-width:75px;' >A3 image</td>
	</tr>
<?
	}

		$i++;
		//if ($i>100) { break; }
		
		$S_image_html = "../../products/images/{$CategoryFolder}/{$ProductNumber}_S_louroe.jpg";
		$L_image_html = "../../products/images/{$CategoryFolder}/{$ProductNumber}_L_louroe.jpg";
		$A1_image_html = "../../products/images/{$CategoryFolder}/{$ProductNumber}_A1_louroe.jpg";
		$A2_image_html = "../../products/images/{$CategoryFolder}/{$ProductNumber}_A2_louroe.jpg";
		$A3_image_html = "../../products/images/{$CategoryFolder}/{$ProductNumber}_A3_louroe.jpg";
		
		
		$L_image = "/home1/louroeco/public_html/products/images/{$CategoryFolder}/{$ProductNumber}_L_louroe.jpg";
		$A1_image = "/home1/louroeco/public_html/products/images/{$CategoryFolder}/{$ProductNumber}_A1_louroe.jpg";
		$A2_image = "/home1/louroeco/public_html/products/images/{$CategoryFolder}/{$ProductNumber}_A2_louroe.jpg";
		$A3_image = "/home1/louroeco/public_html/products/images/{$CategoryFolder}/{$ProductNumber}_A3_louroe.jpg";
		
		$file_L_td = ''; $file_L_td_class = " class='red' ";
		$file_L = ''; if ( file_exists( $L_image ) ) { 
			$file_L = 'Y'; $file_L_td_class = ""; $file_L_td =  $file_L; 
			$file_L_td = "<img src='{$L_image_html}' width=75 height=50 >";	
		} 
		
		$file_A1_td = ''; $file_A1_td_class = " class='red' ";
		$file_A1 = ''; if ( file_exists( $A1_image ) ) { 
			$file_A1 = 'Y'; $file_A1_td_class = ""; $file_A1_td =  $file_A1; 
			$file_A1_td = "<img src='{$A1_image_html}' width=75 height=50 >";	
		} 
		
		$file_A2_td = ''; $file_A2_td_class = " class='red' ";
		$file_A2 = ''; if ( file_exists( $A2_image ) ) { 
			$file_A2 = 'Y'; $file_A2_td_class = ""; $file_A2_td =  $file_A2; 
			$file_A2_td = "<img src='{$A2_image_html}' width=75 height=50 >";	
		} 
		
		$file_A3_td = ''; $file_A3_td_class = " class='red' ";
		$file_A3 = ''; if ( file_exists( $A3_image ) ) { 
			$file_A3 = 'Y'; $file_A3_td_class = ""; $file_A3_td =  $file_A3; 
			$file_A3_td = "<img src='{$A3_image_html}' width=75 height=50 >";	
		} 
		
		

		
		$file_cad = ''; $file_cad_class = " class = 'red' ";
		if ( file_exists( "/home1/louroeco/public_html/products/cad/{$ProductNumber}_louroe.pdf" ) ) { 
			$file_cad = 'Y'; 
			$file_cad_class = " class = 'green' ";
		} 
		
		
		$file_instructions = ''; $file_instructions_class = " class = 'red' ";
		if ( file_exists( "/home1/louroeco/public_html/products/instructions/{$ProductNumber}_louroe.pdf" ) ) { 
			$file_instructions = 'Y'; 
			$file_instructions_class = " class = 'green' ";
		} 

		$file_specifications = ''; $file_specifications_class = " class = 'red' ";
		if ( file_exists( "/home1/louroeco/public_html/products/specifications/{$ProductNumber}_louroe.pdf" ) ) { 
			$file_specifications = 'Y'; 
			$file_specifications_class = " class = 'green' ";
			
		}
		
		$active_td_class = "";  if ($Active=='') { $active_td_class = " class = 'inactive' "; }
?>
	
	<tr <?=$active_td_class?> >
		<td><?=$ProductID?></td>
		<td><?=$Active?></td>
		<td><?=$ProductNumber?></td>
		<td><?=$ProductSerial?></td>
		<td><?=$ProductName?></td>
		<td <?=$file_specifications_class?> ><?=$file_specifications?></td>
		<td <?=$file_instructions_class?> ><?=$file_instructions?></td>
		<td <?=$file_cad_class?> ><?=$file_cad?></td>
		<td>&nbsp;</td>
		<td <?=$file_L_td_class?> ><?=$file_L_td?></td>
		<td <?=$file_A1_td_class?> ><?=$file_A1_td?></td>
		<td <?=$file_A2_td_class?> ><?=$file_A2_td?></td>
		<td <?=$file_A3_td_class?> ><?=$file_A3_td?></td>

	</tr>

<? 
	}
?>
