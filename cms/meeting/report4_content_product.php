<? 

require_once '../../include/data_inc.php';


 $ID = $_GET['ID']; if ($ID=='') $ID=1;

 $sql_product = "select p.*, c.CategoryID, c.CategoryName, c.CategoryFolder from products p 
 left join categories c on p.CategoryID = c.CategoryID
 where p.Active='Y'
 order by c.CategoryID, p.ProductName";
 $mysqli_result_product = mysqli_query($mysqli, "$sql_product"); if (!$mysqli_result_product) { echo 'error in product'; exit; }


$export = true;
if ( $export )
{

	$dt = new DateTime();
	$theDate = date_format($dt, 'Y-m-d');

	$filename ="export_products_{$theDate}.xls";
	
	//header('Content-type: application/ms-excel');
	//header('Content-Disposition: attachment; filename='.$filename);
}
?>
<style>
	td {font-family: "Arial"; font-size: 8pt; border:1px solid #aaa; height:50px; vertical-align: top;}
	table{empty-cells: show; border-collapse:collapse; }

	td.red { background-color: #ffbcbd }
	td.green { background-color: #b8f3d9; }
	.header>td { background-color: #ffeaa6; }
	.inactive>td { background-color: #ddd; }
</style>

<table>

<?

$currCategoryID = '';
$i = 0;
while($row_product = mysqli_fetch_assoc($mysqli_result_product))
{
	foreach ($row_product as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
	if ($currCategoryID<>$CategoryID)
	{
		$currCategoryID = $CategoryID;

		
?>
	<tr class='header'>
		<td colspan=11 ><b><?=$CategoryName?></b><br><br></td>
	</tr>
	<tr class='header'>
		<td>ProductID</td>
		<td>Active</td>
		<td>ProductNumber</td>
		<td>ProductSerial</td>
		<td>ProductName</td>
		<td>Description</td>

	</tr>
<?
	}


?>
	
	<tr <?=$active_td_class?> >
		<td><?=$ProductID?></td>
		<td><?=$Active?></td>
		<td><?=$ProductNumber?></td>
		<td><?=$ProductSerial?></td>
		<td><?=$ProductName?></td>
		<td><?=$Description_Web?></td>

	</tr>

<? 
	}
?>
