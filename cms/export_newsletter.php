<? 

require_once '../include/data_inc.php';

$export = false; if ( isset ($_GET['export']) ) { $export = true; }

$sql = "select Email, Active, DateCreated from newsletter order by Active, Email ";
$mysqli_result = mysqli_query($mysqli, "$sql"); if (!$mysqli_result) { echo 'error in newsletter '; exit; }

$finfo = mysqli_fetch_fields($mysqli_result);
foreach ($finfo as $val) { $field_names[] = $val->name; }
$num_fields = count($field_names);

if ( $export )
{

	$dt = new DateTime();
	$theDate = date_format($dt, 'Y-m-d');

	$filename ="export_newsletter_{$theDate}.xls";
	
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
}
else
{
	echo "<a href='index.php' >Menu</a> / <a href='export_newsletter.php?export'>Export</a><br><br>";
}

?>

<style>
	table { margin: 5px; border-collapse:collapse; padding: 3px; empty-cells:show; border: 1px #ccc solid; }
	td, thead { font-family: "Trebuchet MS"; font-size: 9pt; padding: 3px; border: 1px #ccc solid;  }
	thead { background: #fc9; font-weight:bold; }
	a { text-decoration: none; font-family: "Trebuchet MS"; font-size: 9pt; }
</style>

<table border=1>
    <thead>
     <tr>
       <?
        for ( $i = 0; $i < $num_fields; $i++ )
         { $fieldname = $field_names[$i]; $fieldname = str_replace('_',' ',$fieldname); echo "\n<th>{$fieldname}</th>"; }
       ?>
     </tr>
    </thead>
    <tbody>
	<?
		$item=0;
	    while($row = mysqli_fetch_assoc($mysqli_result))
    	{
    		$item++;
    		$bgColor = '#ecf3fe'; if ($item % 2) { $bgColor = '#ffffff'; }
    	
    		echo "\n<tr bgColor='$bgColor' >";
    	    for ( $i = 0; $i < $num_fields; $i++ )
        	{
         	$field_value = $row[$field_names[$i]]; 
         	echo "<td>{$field_value}</td>";
        	}
        	echo "\n</tr>";
      	}
	?>
	</tbody>

</table>



