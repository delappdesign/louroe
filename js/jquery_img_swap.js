
$(document).ready(function(){
  $('#thumbnails img').each(function(){
   $(this).hover(function(){
    var src = $(this).attr('src').replace('thumb_', ''); // take out /thumbs/ before the file name
    $('#big-image').attr({src: src, title: src, alt: src}); 
   });
  });
 });
