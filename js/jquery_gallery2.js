$(document).ready(function(){

 $('ul#thumbs li img').hover(
 
  function() {
   var newBigImage = $(this).attr('src');
   switchImage(newBigImage);
  },
  function() {
   var newBigImage = $('#bigpic img').attr('rel');
   switchImage(newBigImage);
  
  }
 );

  
 function switchImage(newBigImage) {
  var theBigImage = $('#bigpic img');
  
  //theBigImage.fadeOut(250);
  theBigImage.attr('src', newBigImage).fadeIn(0);
 }

});
