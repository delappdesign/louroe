 $(document).ready(function(){
  $('#nav li').hover(function(){
   $('.submenu', this).slideUp(0).stop(true, true).slideDown(200);
  },
  function(){
   $('.submenu', this).css("display", "block").stop(true, true).delay(100).slideUp(100);
  });
 });