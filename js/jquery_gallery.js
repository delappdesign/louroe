$(document).ready(function(){

 $('ul#thumbs li a').hover(
  function() {
   var currentBigImage = $('#bigpic img').attr('src');
   var newBigImage = $(this).attr('href');
   var currentThumbSrc = $(this).attr('rel');
   switchImage(newBigImage, currentBigImage, currentThumbSrc);
  },
  function() {}
 );

  
 function switchImage(imageHref, currentBigImage, currentThumbSrc) {
   
  var theBigImage = $('#bigpic img');
  
  if (imageHref != currentBigImage) {
  
   theBigImage.fadeOut(250, function(){
    theBigImage.attr('src', imageHref).fadeIn(250);
    
    var newImageDesc = $("ul#thumbs li a img[src='"+currentThumbSrc+"']").attr('alt');
    $('div.desc').empty().html(newImageDesc);
   });
   
   
  }
  
 }

});
