<?php

// php mailer 
require_once 'class.phpmailer.php';

class mailWrapper 
{
	
	public $to, $cc, $bcc, $from, $fromName, $subject, $body, $isHTML;
	
	function send() {

		if ($this->isHTML == '') { $this->isHTML = true; }

		$mail = new PHPMailer(true); 
		
		// add each bcc
		
		if ($this->bcc != '') { 
			$aBCC = explode(';',$this->bcc);
			foreach ($aBCC as $bcc) { $mail->AddBCC($bcc, ""); } 
		}
		
		if ($this->cc != '') { 
			$aCC = explode(';',$this->cc);
			foreach ($aCC as $cc) { $mail->AddCC($cc, ""); } 
		}
		
		if ($this->to != '') { 
			$aTo = explode(';',$this->to);
			foreach ($aTo as $to) { $mail->AddAddress($to, ""); } 
		}
		
		$mail->SetFrom($this->from, $this->fromName);
		$mail->Subject = $this->subject;
		$mail->Body = $this->body;
		$mail->IsHTML($this->isHTML);
	
		if(!$mail->Send()) { echo 'error in mail'; }
		$mail->ClearAddresses();
		unset($mail);

	}

}