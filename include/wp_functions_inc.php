<?

function replace_products ( $text )
{
	global $mysqli;
	
	// replace the [ProductNumber] with the link
	$subject = $text;
	$pattern = '/\[[a-z0-9A-Z]*\]/';
	preg_match_all($pattern, $subject, $matches);
	
	$values='';
	foreach($matches[0] as $match)
	{
		$number = $match;
		$number = str_replace('[','',$number); $number = str_replace(']','',$number);
		$values .= "'{$number}',";
	}
	$values = substr($values,0,-1);
	
	if ($values<>'')
	{
	   $sql_product_number = "select p.*, c.CategoryName from products p 
	   left join categories c on p.CategoryID = c.CategoryID
	   where p.ProductNumber in ( $values ) ";
	   //echo "<br>$sql_product_number";
	   $mysqli_result_product_number = mysqli_query($mysqli, $sql_product_number); if (!$mysqli_result_product_number) { echo 'error in product_1'; exit; }
	   
	   // replace
	   foreach($matches[0] as $match)
	   {
			$number = $match;
			$number = str_replace('[','',$number); $number = str_replace(']','',$number);
	   
			if (mysqli_num_rows($mysqli_result_product_number)<>0) mysqli_data_seek($mysqli_result_product_number, 0);
			while($row_product_number = mysqli_fetch_assoc($mysqli_result_product_number))
			{
				if ($row_product_number['ProductNumber'] == $number)
				{
					$ProductID = $row_product_number['ProductID'];
					$ProductName = $row_product_number['ProductName'];
					$ProductSerial = $row_product_number['ProductSerial'];
					$CategoryName = $row_product_number['CategoryName'];
					$CategoryURLName = str_replace(' ','-',$CategoryName);
					
					$link = "<a href='/products-systems/{$CategoryURLName}/{$ProductSerial}'>{$ProductName}</a>";
					$text = str_replace( $match, $link, $text);
					break;
				}
			}
	
	   } 
	}

	return $text;
}

?>