<?
if (session_status() == PHP_SESSION_NONE) { session_start(); }

class clsSecurity
{


	
	private $CSRFToken;
	
    public function __construct() {
    
    	global $mysqli;
    
		foreach ($_POST as $key => $value) {
			//echo "<br>$key = $value";
			$replace_arr = array('\r','\n',"\r","\n",'<script','</script');
			$value = str_replace($replace_arr,'',$value);
			$value = mysqli_real_escape_string($mysqli, stripslashes($value));
			$_POST["$key"] = $value;
		}
		
		foreach ($_GET as $key => $value) {
			//echo "<br>$key = $value";
			$replace_arr = array('\r','\n',"\r","\n",'<script','</script');
			$value = str_replace($replace_arr,'',$value);
			$value = mysqli_real_escape_string($mysqli, stripslashes($value));
			$_GET["$key"] = $value;
		}
    
    }
    
    public function isAuthenticated() {
    
    	// echo $_POST['CSRF'] . ' == ' . $_SESSION['CSRF'] .'<br>';
    	$CSRFAuthenticate = false;
    	if (isset($_POST['CSRF'])) {
			if ( ($_POST['CSRF'] == $_SESSION['CSRF']) && ($_SESSION['CSRF'] != '' && $_POST['CSRF'] != '') ) { $CSRFAuthenticate = true; }
		}
		return $CSRFAuthenticate;
    }
    
    public function setCSRF() {
    
    	$CSRFKey = "MBDA"; $RequestTime = time();
		$this->CSRFToken= sha1($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].$_SERVER['REQUEST_URI'].$RequestTime.$CSRFKey);
		$_SESSION['CSRF'] = $this->CSRFToken;
		
    }
    
    public function writeTagCSRF() {
    	return "<input type=hidden name=CSRF value='{$this->CSRFToken}' >";
    }
}

$thisSecurity = new clsSecurity();

?>