<?php
$RepFirm = htmlspecialchars($_POST["element_3"]);
$month = htmlspecialchars($_POST["element_1_1"]);
$day = htmlspecialchars($_POST["element_1_2"]);
$year = htmlspecialchars($_POST["element_1_3"]);
$First = htmlspecialchars($_POST["element_2_1"]);
$Last = htmlspecialchars($_POST["element_2_2"]);
$company = htmlspecialchars($_POST["element_4"]);
$custtype = htmlspecialchars($_POST["element_5"]);
$product = htmlspecialchars($_POST["element_7"]);
$notes = htmlspecialchars($_POST["element_6"]);
$base_url = "https://louroe.sugarondemand.com/rest/v10";
$username = "Joel";
$password = "6955Valjean";


/**
 * Generic function to make cURL request.
 * @param $url - The URL route to use.
 * @param string $oauthtoken - The oauth token.
 * @param string $type - GET, POST, PUT, DELETE. Defaults to GET.
 * @param array $arguments - Endpoint arguments.
 * @param array $encodeData - Whether or not to JSON encode the data.
 * @param array $returnHeaders - Whether or not to return the headers.
 * @return mixed
 */
function call(
    $url,
    $oauthtoken='',
    $type='GET',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= "?" . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "PUT");
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}

//Login - POST /oauth2/token

$url = $base_url . "/oauth2/token";

$oauth2_token_arguments = array(
    "grant_type" => "password",
    //client id/secret you created in Admin > OAuth Keys
    "client_id" => "1234567890",
    "client_secret" => "test_secret",
    "username" => $username,
    "password" => $password,
    "platform" => "base"
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments);

//Create record - POST /<module>/

/******* 3/7/2015 Fix for Web Form *********/
$url = $base_url . "/Notes";


if(!empty($day)&&!empty($month)&&!empty($year))
$date_of_visit_c = $year."-".$month."-".$day;
$record_arguments = array(
    "name" => "Outside Sales Call",
    "description" => $notes,
	"company_visited_c" => $company,
	"customer_type_c" => $custtype,
	"date_of_visit_c" => $date_of_visit_c,
	"product_of_most_interest_c" => $product,
	"rep_firm_c" => $RepFirm,
	"sales_person_c" => $First." ".$Last
);

/********************/

$record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);

/* echo "<pre>";
print $oauth2_token_response->access_token;
print_r($record_arguments);
print_r($record_response);
echo "</pre>"; */
print "Form submitted successfully!";
?>