<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
	// Enable features from Soil when plugin is activated
	// https://roots.io/plugins/soil/
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
	add_theme_support( 'soil-jquery-cdn' );
	add_theme_support( 'soil-relative-urls' );

	// Add WooCommerce Support
	add_theme_support( 'woocommerce' );

	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations
	load_theme_textdomain( 'sage', get_template_directory() . '/lang' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus( [
		'primary_navigation' => __( 'Primary Navigation', 'sage' ),
		'footer_navigation'  => __( 'Footer Navigation', 'sage' )
	] );

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	// Enable post formats
	// http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', [ 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ] );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	// Use main stylesheet for visual editor
	// To add custom styles edit /assets/styles/layouts/_tinymce.scss
	add_editor_style( Assets\asset_path( 'styles/main.css' ) );

	// Add image sizes - featured image (banner)
	add_image_size( 'banner-xl-2x', 2420 );
	add_image_size( 'banner-xl', 1210 );
	add_image_size( 'banner-lg-2x', 2004 );
	add_image_size( 'banner-lg', 1002 );
	add_image_size( 'banner-md-2x', 1556 );
	add_image_size( 'banner-md', 778 );
	add_image_size( 'banner-sm-2x', 1172 );
	add_image_size( 'banner-sm', 586 );
	add_image_size( 'product-thumbnail', 150, 150 );
	add_image_size( 'product-thumbnail-sm', 100, 100 );
	add_image_size( 'page-product-img', 300, 300 );
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

/**
 * Register sidebars
 */
function widgets_init() {

	register_sidebar( [
		'name'          => __( 'Primary', 'sage' ),
		'id'            => 'sidebar-primary',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	] );

	register_sidebar( array(
		'name'         => __( 'Product Sidebar' ),
		'id'           => 'product-sidebar',
		'class'        => 'sidebar-tab',
		'description'  => __( 'Widgets in this area will be shown on product pages.' ),
		'before_title' => '<h3>',
		'after_title'  => '</h3>'
	) );

	register_sidebar( array(
		'name'         => __( 'Slide-out Tab' ),
		'id'           => 'slide-out-tab',
		'description'  => __( 'Widgets in this area will be shown on the Slide-out Tab.' ),
		'before_title' => '<h4>',
		'after_title'  => '</h4>'
	) );

}

add_action( 'widgets_init', __NAMESPACE__ . '\\widgets_init' );

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
	global $is_full_screen;
	static $display;

	isset( $display ) || $display = ! in_array( true, [
		// The sidebar will NOT be displayed if ANY of the following return true.
		// @link https://codex.wordpress.org/Conditional_Tags
		is_404(),
		is_search(),
		is_front_page(),
		is_page_template( 'template-custom.php' ),
		$is_full_screen
	] );

	return apply_filters( 'sage/display_sidebar', $display );
}

/**
 * Theme assets
 */
function assets() {

	$theme_version = wp_get_theme()->get('Version');

	wp_enqueue_style( 'fontawesome-css', Assets\asset_path( 'css/font-awesome.min.css' ), false, $theme_version );
	wp_enqueue_style( 'sage/css', Assets\asset_path( 'css/main.css' ), array('fontawesome-css'), $theme_version );
	wp_enqueue_style( 'google_fonts', '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i', false, null );

	if ( is_front_page() ) {
		wp_enqueue_style( 'flexslider_css', Assets\asset_path( 'css/flexslider.css' ), false, $theme_version );
	}


	if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_front_page() ) {
		wp_enqueue_script( 'flexslider_js', get_template_directory_uri() . '/assets/scripts/jquery.flexslider-min.js', [ 'jquery' ], $theme_version, true );
	}

	if ( is_page( 'rma-application' ) ) {
		wp_enqueue_script( 'jquery-qtip', get_template_directory_uri() . '/assets/scripts/jquery.qtip-1.0.0-rc3.min.js', [ 'jquery' ], '1.0.0-rc3', true );
		wp_enqueue_script( 'louroe-rma-js', get_template_directory_uri() . '/assets/scripts/rma.js', [
			'jquery',
			'jquery-qtip'
		], $theme_version, true );
	}

	//js-cookie is deprecated, but still used by woo commerce, so check to see if it is used, and then add
	if( wp_script_is('js-cookie')){
		wp_enqueue_script( 'js-cookie', Assets\asset_path( 'scripts/js.cookie.min.js' ), null, '2.2.0', true );
	}
	wp_enqueue_script( 'tether-js', Assets\asset_path( 'scripts/tether.min.js' ), null, '1.4.0', true );
	wp_enqueue_script( 'bootstrap-js', Assets\asset_path( 'scripts/bootstrap.min.js' ), [ 'jquery', 'tether-js' ], '4.0.0-alpha.4', true );
	wp_enqueue_script( 'sage/js', Assets\asset_path( 'scripts/main.js' ), [ 'jquery', 'hoverIntent', 'js-cookie', 'bootstrap-js' ], $theme_version, true );

}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );
