<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
function title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'sage');
    }

  } elseif (is_product_category() ) {
	  return woocommerce_page_title();

  } elseif (is_post_type_archive(['tribe_events'])){
  	if (function_exists('get_field')){
  		$events_title = get_field('events_page_title', 'options');
	  }
  	return $events_title ? $events_title : "Events";

  } elseif (is_archive()) {
    return get_the_archive_title();

  } elseif (is_search()) {
    return sprintf(__('Search Results for <em>%s</em>', 'sage'), get_search_query());

  } elseif (is_404()) {
    return __('Not Found', 'sage');

  } else {
    return get_the_title();
  }
}
