<?php

namespace Roots\Sage;

/** 
 * Detect Browser & Features
 * 
 * Thanks:
 * <link>http://www.wpbeginner.com/wp-themes/how-to-add-user-browser-and-os-classes-in-wordpress-body-class/</link>
 * 
 * @author Patrick Jackson <PJackson@GoldenPathSolutions.com>
 * @version 1.0.0
 */

function browser_body_class($classes) {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_edge, $is_iphone, $is_macIE, $is_winIE;
        if ( $is_lynx) $classes[] = 'lynx';
        elseif ( $is_gecko ) $classes[] = 'gecko';
        elseif ( $is_opera ) $classes[] = 'opera';
        elseif ( $is_NS4 ) $classes[] = 'ns4';
        elseif ( $is_safari ) $classes[] = 'safari';
        elseif ( $is_chrome ) $classes[] = 'chrome';
        elseif ( $is_edge ) $classes[] = 'edge';
        elseif( $is_IE ) {
                $classes[] = 'ie';
                
                if (preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version)){
                    
                    $classes[] = 'ie'.$browser_version[1];
                    
                } elseif (preg_match('/(?i)msie [10]/',$_SERVER['HTTP_USER_AGENT'])){
                    
                    $classes[] = 'ie10';
                    
                } elseif (preg_match("/Trident\/7.0;(.*)rv:11.0/", $_SERVER["HTTP_USER_AGENT"] )){
                    
                    $classes[] = 'ie11';
                    
                }
                
        } else $classes[] = 'unknown';
        
        if($is_iphone) $classes[] = 'iphone';
        if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
                 $classes[] = 'osx';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
                 $classes[] = 'linux';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
                 $classes[] = 'windows';
           }
           
        if ( $is_macIE ) $classes[] = 'macIE';
        elseif ( $is_winIE ) $classes[] = 'winIE';
        
        
        return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\browser_body_class');