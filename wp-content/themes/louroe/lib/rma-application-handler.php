<?php
namespace Roots\Sage\RMA;

add_action ('template_redirect', __NAMESPACE__ . '\\handle_rma_application');
function handle_rma_application() {

	if ( ! is_page('rma-application' ) ) {
		return;
	}

	require_once 'include/wp_data_inc.php';
	require_once 'include/mail_inc.php';


	$timestamp = date( 'U' );

	$pageVariables = 'action,wrmaid,rmaid,rmakey,company,address1,address2,city,state,zip,contactName,phone,fax,email,model1,qty1,sn1,model2,qty2,sn2,model3,qty3,
sn3,model4,qty4,sn4,model5,qty5,sn5,problem,repairReturn,return';
	$arrayVariables = explode(",",str_replace(' ','',$pageVariables));
	foreach ($arrayVariables as $value) { $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }

	if ($action == 'submit') {

		$sql                   = "insert into rma (Company, RMARequestDate) values ('$Company', now()) ";
		$mysqli_result_product = mysqli_query( $mysqli, $sql );
		if ( ! $mysqli_result_product ) {
			echo 'error in rma insert';
			exit;
		}
		$WRMAID = mysqli_insert_id( $mysqli );

		$RMAKey = randString( 4 ) . '-' . randString( 4 );

		$repairReturn = ( $repairReturn != '' ) ? 'Y' : '';
		$return       = ( $return != '' ) ? 'Y' : '';

		$sql = "update rma set RMAKey = '$RMAKey', Company = '$company', Address1 = '$address1', Address2 = '$address2',
	City = '$city', State = '$state', Zip = '$zip',
	ContactName = '$contactName', Phone = '$phone', Fax = '$fax', Email = '$email', Model1 = '$model1', Qty1 = '$qty1', SerialNo1 = '$sn1',
	Model2 = '$model2', Qty2 = '$qty2', SerialNo2 = '$sn2', Model3 = '$model3', Qty3 = '$qty3', SerialNo3 = '$sn3', 
	Model4 = '$model4', Qty4 = '$qty4', SerialNo4 = '$sn4', Model5 = '$model5', Qty5 = '$qty5', SerialNo5 = '$sn5',
	Problem = '$problem', RepairAndReturn = '$repairReturn', ReturnForCredit = '$return', RMAStatus = 'Request-Entered'
	where WRMAID = '$WRMAID' ";

		$mysqli_result_product = mysqli_query( $mysqli, $sql );
		if ( ! $mysqli_result_product ) {
			echo 'error in rma update';
			exit;
		}

		// send email confirmation

		$message = "Thank you for submitting an RMA with Louroe Electronics.<br><br>";
		$message .= "Your RMA Key is: $RMAKey <br><br> ";
		$message .= "<a href='https://www.louroe.com/support/rma-portal'>Click here to check the status of your RMA.</a>";


		try {
			$mail          = new \mailWrapper();
			$mail->subject = 'Louroe RMA Application';

			$to       = $email;
			$mail->cc = 'rma@louroe.com; orderdesk@louroe.com';
			//$mail->bcc = 'rob@robcsoftware.com';

			$mail->body     = $message;
			$mail->to       = $to;
			$mail->from     = "rma@louroe.com";
			$mail->fromName = 'Louroe Info';

			$mail->send();
		} catch ( Exception $e ) {
			echo "There was an error sending your notification email: " . $e->getMessage();
		}

		$redirect = get_site_url() . '/support/rma-submit/';
		wp_redirect( $redirect );
		exit;

	}

}


/**
 * Add custom redirect
 */
function custom_redirect() {
	if ( is_page('rma-application') && isset( $_POST['action'] ) && ( 'submit' === $_POST['action'] ) ) {

	}
}

/**
 * Get Random string
 * @param $length
 * @param string $charset
 *
 * @return string
 */
function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
	$str = '';
	$count = strlen($charset)-1;
	while ($length--) {
		$str .= $charset[mt_rand(0, $count)];
	}
	return $str;
}