<?php

namespace Roots\Sage\RMA;

add_action ('template_redirect', __NAMESPACE__ . '\\handle_rma_portal_login');
function handle_rma_portal_login() {

	if ( ! is_page('rma-portal' ) ) {
		return;
	}

	session_start();

	require_once 'include/wp_data_inc.php';

	$pageVariables = 'action,email,rmakey';
	$arrayVariables = explode(",",str_replace(' ','',$pageVariables));
	foreach ($arrayVariables as $value) { $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }

	$error = '';

	if ($email != '' && $rmakey != '') {

		$sql = "select * from rma where Email = '{$email}' and RMAKey = '{$rmakey}' " ;
		$mysqli_result_content = mysqli_query($mysqli, "$sql"); if (!$mysqli_result_content) { echo 'error in rma login'; exit; }
		$row = mysqli_fetch_assoc($mysqli_result_content);
		$Email = $row['Email'];
		if ( $Email != '' ) {
			$_SESSION['RMAEmail'] = $Email;
			$redirect = get_site_url() . '/support/rma-portal-main';
			wp_redirect( $redirect );
			exit;
		}

		$error = "<br>There are no RMA records for this email address";

	}

}

