<?php

namespace Roots\Sage\WooCommerce;


/**
 * Custom WooCommerce functions
 *
 * @author Patrick Jackson <pjackson@goldenpathsolutions.com>, Christine Estrada via DeLapp Design
 * @version 1.0.0
 * @since 1.0
 */

//Global Variables for Product Archive Listings by Sub-Category
global $prev_product_cat;
$prev_product_cat = false;

/**
 * Order Product Archive Query
 *
 * Adapted from old theme, Adapted from old theme, thanks Christine Estrada!
 *
 * @since 1.0
 */
function order_product_list() {

	//Setup vars
	global $parent_category;
	$queried_object = get_queried_object();
	$GLOBALS['parent_category'] = get_term_by('slug', $queried_object->slug, 'product_cat');
	//echo $parent_category->name;
	$sub_categories = get_terms( 'product_cat', array( 'parent' => $parent_category->term_id, 'orderby' => 'slug' ) );
	$posts_by_sub_categories = array();

	//Return if no sub categories
	if ( empty($sub_categories) || ($queried_object->slug == "accessories") ) { return; }

	//Get all sub categories of archive category, create empty array for each
	foreach ($sub_categories as $sub_cat) {
		$posts_by_sub_categories[$sub_cat->name] = array();
	}

	//Loop through each post
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			//Get the post ID and its categories
			$product_id = get_the_ID();
			$product_cats = get_the_terms( $product_id, 'product_cat' );
			//Loop through its categories, find which subcategory of $parent_category the post belongs to
			foreach ($product_cats as $cat) {
				if ( $cat->parent == $parent_category->term_id ) {
					$posts_by_sub_categories[$cat->name][] = $product_id;
				}
			}
		} // end while
	} // end if

	//Append subcategory post arrays to one product list
	$ordered_product_list = array();
	foreach ($posts_by_sub_categories as $sub_category) {
		$ordered_product_list = array_merge($ordered_product_list, $sub_category);
	}

	//Generate new query with ordered products
	$ordered_query_args = array(
		'post_type'=> 'product',
		'posts_per_page' => -1,
		'post__in' => $ordered_product_list,
		'orderby'    => 'post__in'
	);

	query_posts( $ordered_query_args );

}

/**
 * Adapted from old theme, Adapted from old theme, thanks Christine Estrada!
 *
 * @since 1.0
 */
function reset_previous_product_cat() {
	$GLOBALS['prev_product_cat'] = false;
	return;
}

function check_if_new_category() {

	//Setup global vars
	global $prev_product_cat;
	global $parent_category;

	//Return if Accessories (only 1 subcategory)
	if ( $parent_category->name == "Accessories" ) { return; }

	//Setup vars
	$product_id = get_the_ID();
	$product_cats = get_the_terms( $product_id, 'product_cat' );

	//Get parent category ID
	$parent_cat_title = woocommerce_page_title(false);
	//$parent_cat_term = get_term_by( 'name', $parent_cat_title, 'product_cat');
	$parent_cat_id = $parent_category->term_id;

	//If no previous product category, this is the start of the product list - list header for current category.
	if ( $prev_product_cat == false ) {
		foreach ($product_cats as $cat) {
			if ( $cat->parent == $parent_cat_id ) {
				$GLOBALS['prev_product_cat'] = $cat->term_id;
				return "<h3>".$cat->name."</h3>";
			}
		}
	} else {
		//Loop through the current product's sub-categories, find sub-category that is child of category
		foreach ($product_cats as $cat) {
			if ( ($cat->parent == $parent_cat_id) && ($GLOBALS['prev_product_cat'] != $cat->term_id) ) {
				$GLOBALS['prev_product_cat'] = $cat->term_id;
				return '</div></ul>
					<div class="product-list group">
						<ul class="products">
							<h3>'.$cat->name.'</h3>';
			}
		}
	}
	return;
}