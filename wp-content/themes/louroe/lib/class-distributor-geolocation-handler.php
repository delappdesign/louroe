<?php
namespace Roots\Sage\Distributor;

/**
 * Distributor Geolocation Handler
 *
 * Handles visitor geolocation and pulling distributor location
 *
 * @author Patrick Jackson <pjackson@goldenpathsolutions.com>
 * @version 1.0.0
 * @since 2.1.0 Theme version
 */

class Distributor_Geolocation_Handler{

	protected $current_location;

	/**
	 * Distributor_Geolocation_Handler constructor.
	 *
	 * sets $current_location
	 *
	 * @param string        $ip IP Address overrides current location.  Uses current IP Address when null.
	 *
	 * @since 1.0.0
	 */
	function __construct( $ip = null ) {

		if (function_exists('\geoip_detect2_get_info_from_ip') ) {
			$this->current_location = $ip ? \geoip_detect2_get_info_from_ip( $ip ) : \geoip_detect2_get_info_from_current_ip();
		}

	}

	/**
	 * @return \stdClass|\YellowTree\GeoipDetect\DataSources\City
	 */
	public function get_current_location(){

		return $this->current_location;

	}

	/**
	 * @return array|null When found, returns an associative array with Distributor's information
	 */
	public function get_nearest_distributor(){
		return $this->get_nearest_distributors( 1 );
	}

	/**
	 * Get nearest distributor to this handler's stored location
	 *
	 * @param $number   Number of distributors to return
	 * @return array|null   When found, returns an associative array with Distributors' information
	 */
	public function get_nearest_distributors( $number ){

		global $mysqli;

		require_once 'include/wp_data_inc.php';

		$latitude = $this->get_current_location()->location->latitude;
		$longitude = $this->get_current_location()->location->longitude;
		$distributors = null;

		$sql = $this->get_sql($latitude, $longitude, $number);
		$mysqli_result = mysqli_query($mysqli, "$sql");
		if ( ! $mysqli_result ){
			error_log("Failed to get a distributor from the database.  Here's the SQL: " . $sql);
		} else {

			// fetch rows
			if ( 1 === $number ) {
				$distributors = mysqli_fetch_assoc( $mysqli_result );
			} else {

				while( $row = mysqli_fetch_assoc( $mysqli_result ) ){
					$distributors[] = $row;
				}

			}

		}

		return $distributors;
	}

	/**
	 * @param $latitude     latitude from which to find nearest distributor(s)
	 * @param $longitude    longitude from which to find nearest distributor(s)
	 * @param int $number   number of distributors to return (default: 1)
	 *
	 * @return string
	 */
	private function get_sql($latitude, $longitude, $number = 1){

		$sql = "SELECT d.*, ";
		$sql .= " ( degrees( acos( sin( radians( Latitude ) ) * sin( radians( $latitude ) ) + cos( radians( Latitude ) ) * cos( radians( $latitude ) ) * cos( radians( Longitude - $longitude ) ) ) ) * 60 * 1.1515 ) as Distance ";
		$sql .= "FROM distributors d ";
		$sql .= "WHERE d.Active='Y' ";
		$sql .= "ORDER BY Distance LIMIT $number";

		return $sql;
	}


}