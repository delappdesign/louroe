<?php
namespace Roots\Sage\RMA;

add_action ('template_redirect', __NAMESPACE__ . '\\handle_rma_portal_main');
function handle_rma_portal_main() {

	global $result;

	if ( ! is_page('rma-portal-main' ) ) {
		return;
	}

	session_start();

	require_once 'include/wp_data_inc.php';

	$email = $_SESSION['RMAEmail'];
	$sql = " select * from rma where Email = '{$email}' order by wrmaid ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing'; exit; }

	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {

		$datetime = strtotime($row['RMARequestDate']);
		$dateformatted = date("m-d-Y", $datetime);
		$row['RMARequestDate'] = $dateformatted;
		$wrmaid = $row['WRMAID'];

		// get rma details
		$sql = " select * from rma_details where wrmaid = '$wrmaid' order by WRMA_DetailID ";
		$mysqli_result_details = mysqli_query($mysqli, $sql); if (!$mysqli_result_details) { echo 'error in listing'; exit; }

		$details = array();

		while ( $row_details = mysqli_fetch_assoc($mysqli_result_details) ) {

			$datetime = strtotime($row_details['CreateDate']);
			$dateformatted = date("m-d-Y", $datetime);
			$row_details['CreateDate'] = $dateformatted;

			$details[] = $row_details;
		}

		$row['rma-details'] = $details;

		$result[] = $row;

	}


}
