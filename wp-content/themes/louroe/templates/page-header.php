<?php

use Roots\Sage\Titles;

global $is_full_screen, $post, $parent;

if ( function_exists('get_field') ){
	$subtitle = get_field('lou_subtitle');
}

// is this an industries page?
$parent = $post ? get_post( $post->post_parent ) : null;
$is_industry = $parent && ('industries' === $parent->post_name);

?>

<section class="page-header <?= $is_full_screen ? 'container' : '' ?>">

  <h1 class="<?= empty($subtitle) ? 'no-subtitle' : '' ?>"><?= Titles\title(); ?>
	  <?php if ($is_industry):?>
		  <?php get_template_part('templates/components/dropdown', 'industry-menu'); ?>
	  <?php endif; ?>
  </h1>

	<?php if (! empty( $subtitle ) ): ?>
		<h2><?= $subtitle ?></h2>
	<?php endif; ?>

</section>


