<?php
/**
 * Handles geolocation detection and finding nearest distributor
 */

global $nearest_distributor;

if ( ! empty( $nearest_distributor ) ){
	$name = ! empty( $nearest_distributor['CompanyName'] ) ? $nearest_distributor['CompanyName'] : null;
	$address = ! empty( $nearest_distributor['BillingAddress'] ) ? $nearest_distributor['BillingAddress'] : null;
	$city = ! empty( $nearest_distributor['City'] ) ? $nearest_distributor['City'] : null;
	$state = ! empty( $nearest_distributor['StateOrProvince'] ) ? $nearest_distributor['StateOrProvince'] : null;
	$zip = ! empty( $nearest_distributor['PostalCode'] ) ? $nearest_distributor['PostalCode'] : null;
	$phone = ! empty( $nearest_distributor['PhNumber'] ) ? $nearest_distributor['PhNumber'] : null;
	$url = ! empty( $nearest_distributor['WebsiteAddress'] ) ? $nearest_distributor['WebsiteAddress'] : null;

	$full_address = $address . '; ' . $city . ', ' . $state . ' ' . $zip . ' ' . $country;
}

// DEBUG
/*echo "<pre>";
var_dump($geo_handler->get_current_location());
var_dump($nearest_distributor);
echo "</pre>";*/

?>
<?php if ($nearest_distributor) : ?>
<div class="geolocation">

	<div class="distributor-profile" itemscope itemtype="http://schema.org/LocalBusiness">

		<?php if ($name) : ?>
			<?php if ( $url ) : ?><a itemprop="url" href="<?= strpos($url,'http') ? $url : "http://" . $url ?>" target="_blank"><?php endif; ?>
				<p class="name" itemprop="name"><?= $name ?></p>
			<?php if ($url) : ?></a><?php endif; ?>
		<?php endif; ?>

		<?php if ($address || $city || $state || $zip):?>
			<a href="https://maps.google.com/?q=<?= htmlspecialchars($full_address, ENT_QUOTES); ?>" target="_blank">
				<ul itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

						<?php if ($address) : ?><li class='address'><span itemprop="streetAddress"><?= $address ?></span></li><?php endif; ?>
						<?php if ($city || $state || $zip) : ?>
							<li class='city-state-zip'>
									<?php if ($city):?><span itemprop="addressLocality"><?= $city ?></span><?php endif; ?><?=
									$city && $state ? ', ' : ' ' ?>
									<?php if ($state):?><span itemprop="addressRegion"><?= $state ?></span><?php endif; ?>
									<?= $state && $zip ? ' ' : ''?>
									<?php if ($zip):?><span itemprop="postalCode"><?= $zip ?></span><?php endif;?>
							</li>
						<?php endif; ?>

				</ul>
			</a>
		<?php endif; ?>

		<ul>
			<?php if ( $phone ) : ?><li class='phone'><a href='tel:<?= preg_replace('/\D+/', '', $phone); ?>' itemprop="telephone"><?= $phone ?></a></li><?php endif; ?>
		</ul>

	</div>
</div>
<?php endif; ?>