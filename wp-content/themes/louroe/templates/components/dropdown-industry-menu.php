<?php
/**
 * popup with list of industry links
 */
global $parent;
?>


<nav id="browse-other-wrapper" class="dropdown" role="navigation">
	<a class="browse-other-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Browse Other Industries</a>
	<ul id="browse-other-list" class="dropdown-menu" aria-labelledby="browse-other-link">
		<?php wp_list_pages( [ 'child_of' => $parent->ID, 'title_li' => null ] ); ?>
	</ul>
</nav>
