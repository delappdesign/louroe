<?php
use \Roots\Sage\Distributor;

global $nearest_distributor;

$geo_handler = new Distributor\Distributor_Geolocation_Handler();
if ( $geo_handler ){
	$current_city = $geo_handler->get_current_location()->city->name;
	$latitude = $geo_handler->get_current_location()->location->latitude;
	$longitude = $geo_handler->get_current_location()->location->longitude;
	$nearest_distributors = $geo_handler->get_nearest_distributors( 3 );
}

if ( function_exists('get_field') ){
	$local_distributor_heading = get_field('local_distributor_heading', 'options');
}

?>


<div class="cta-buttons <?= $nearest_distributors ? 'has-distributor' : '' ?>">

	<?php if ( $nearest_distributors): ?>
		<div class="distributors-header">
			<?php if ( $local_distributor_heading): ?><h2><?= $local_distributor_heading ?></h2><?php endif; ?>

			<div class="distributors">
				<?php foreach( $nearest_distributors as $nearest_distributor ): ?>
					<?php get_template_part('templates/components/product', 'geolocation'); ?>
				<?php endforeach; ?>
			</div>

		</div>
	<?php endif; ?>

	<?php $where_to_buy_copy = $nearest_distributor ? "More Distributors" : "Where to Buy"; ?>
	<a href="/about-us/distributors/" class="cta-button-secondary <?= $nearest_distributors ? 'has-distributor' : '' ?>"><?= $where_to_buy_copy?></a>
	<a href="/contact/" class="cta-button-secondary">Contact us</a>
</div>

<hr />

<ul class="sidebar-tab">

	<?php $docs = get_field('product_documentation');
	if($docs):?>

		<li><a href="#"><span class="sidebar-icon">+</span> Documentation </a>
			<div class="sidebar-div">
				<?php foreach($docs as $doc) {
					$output = '<ul class="ul">';
					if($doc['pdf']) {
						$output .='<li><a href="'.$doc['pdf'].'" target="_blank">'.$doc['link_name'].' (PDF)</a></li>';
					} else {
						$output .='<li><a href="'.$doc['url'].'" target="_blank">'.$doc['link_name'].'</a></li>';
					}
					$output .= '</ul>';
					echo $output;
				} ?>
			</div>
		</li>
	<?php endif; ?>
	<?php $compats = get_field('product_compatibility');
	if($compats):?>

		<li><a href="#"><span class="sidebar-icon">+</span> Compatibility </a>
			<div class="sidebar-div">
				<?php echo $compats ?>
			</div>
		</li>
	<?php endif; ?>

	<?php $case = get_field('product_casestudy');
	if($case):?>

		<li><a href="#"><span class="sidebar-icon">+</span> Case Study </a>
			<div class="sidebar-div">
				<?php echo $case ?>
			</div>
		</li>
	<?php endif; ?>

	<?php $included = get_field('product_includedin');
	if($included):?>

		<li><a href="#"><span class="sidebar-icon">+</span> Included In </a>
			<div class="sidebar-div">
				<?php echo $included ?>
			</div>
		</li>

	<?php endif; ?>
</ul>
<?php $video = get_field('product_video');
if($video):?>
	<hr />
	<div>
		<h4><strong>Video</strong></h4>
		<?php echo $video ?>
	</div>
<?php endif; ?>
