<?php
/**
 *  Slide-out Tab
 *
 *  Gixed position tab that slides out when clicked.
 *  Contents are dictated by widget area.
 *
 * @author  Patrick Jackson <pjackson@goldenpathsolutions>
 * @version 1.0.0
 */

if( function_exists('get_field') ){
	$slide_out_tab_title = get_field('slide-out_tab_title', 'options');
}
?>

<div id="slide-out-tab-wrapper">
	<h3 class="title"><?= $slide_out_tab_title ?><span class="opener fa fa-angle-up" title="open"></span><span class="closer fa fa-angle-down" title="close"></span></h3>
	<div class="slide-out-tab-content">
		<?php dynamic_sidebar('slide-out-tab'); ?>
	</div>
</div>
