<?php

use Roots\Sage;

global $post;

if ( function_exists( 'get_field' ) ){


	// Top header theme options
	$header_contact = get_field("header_contact", "options");
	$show_translation = get_field("show_translation", "options");
	$show_search = get_field("show_search", "options");
	$show_mia = get_field("show_mia", "options"); // show Made In America (MIA)
	$mia_copy = get_field("mia_copy", "options");

	// Main header theme options
	$logo_svg =  get_field( "logo_svg", "options" );

}

?>

<header class="banner">

	<div id="top-header" class="desktop hidden-md-down">

		<div class="container">

				<?php if ( $header_contact ) :?>
					<p class="contact"><?= $header_contact ?></p>
				<?php endif; ?>

				<?php if ( $show_translation ) :?>
					<?php
					$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
					$permalink = empty( $_SERVER['HTTP_HOST'] ) || empty( $_SERVER['REQUEST_URI'] ) ? get_site_url() : $protocol . "://" .$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					$translate_target = urlencode( $permalink );
					?>

					<a class="translate" href="https://translate.google.com/translate?sl=en&tl=es&js=y&prev=_t&hl=en&ie=UTF-8&u=<?= $translate_target; ?>&edit-text=&act=url">En Español</a>
				<?php endif; ?>

				<?php if ( $show_search ) : ?><?= get_search_form() ?><?php endif; ?>

				<?php if ($show_mia) : ?>
						<div id="made-in-america">
							<img src="<?= get_template_directory_uri(); ?>/assets/images/Made_in_USA-logo_100x50.gif"
							     srcset="<?= get_template_directory_uri(); ?>/assets/images/Made_in_USA-logo_100x50.gif 1x,
			                       <?= get_template_directory_uri(); ?>/assets/images/Made_in_USA-logo_200x100.gif 2x">
							<p><?= $mia_copy; ?></p>
					</div>
				<?php endif; ?>

		</div>

	</div>


  <div id="main-header">

	  <div class="container">

		  <div class="row">

	      <!-- desktop logo -->
	      <div class="header-branding col-xs-4 col-lg-2" itemscope itemtype="http://schema.org/Organization">
	          <a class="brand" href="<?= esc_url(home_url('/')); ?>">
	              <img itemprop="logo" src="<?= $logo_svg['url'] ?>" alt="<?= $logo_svg['alt'] ?>">
	          </a>
	      </div>

			  <div class="mobile buttons hidden-lg-up col-xs-8">

		      <nav class="navbar mobile-nav-button">
			      <?php if ( $show_search ) : ?><button class="search-btn fa fa-search"></button><?php endif; ?>
			      <button class="navbar-toggler opener" type="button">☰</button>
		      </nav>

				  <?php if ( $show_search ) : ?>

					  <div class="search-panel">
					    <a class="close-btn fa fa-times"></a>
					    <?= get_search_form() ?>
					  </div>

				  <?php endif; ?>

			  </div>

			  <nav class="nav-primary desktop hidden-md-down col-lg-10" role="navigation"
			       itemscope itemtype="http://schema.org/SiteNavigationElement">

				  <?php
				  if (has_nav_menu('primary_navigation')) :
					  wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new Sage\wp_bootstrap_navwalker_mega_menu()]);
				  endif;
				  ?>
			  </nav>

		  </div>

	  </div>

  </div>
</header>

<nav class="nav-primary mobile-nav sidenav hidden-lg-up" role="navigation"
     itemscope itemtype="http://schema.org/SiteNavigationElement">
    <button class="navbar-toggler closer" type="button">Close</button>
	<?php
	if (has_nav_menu('primary_navigation')) :
		wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new Sage\wp_bootstrap_navwalker()]);
	endif;
	?>
</nav>