<?php use Roots\Sage\WooCommerce; ?>

<?php get_template_part( 'templates/page', 'header' ); ?>

<section class="container">

	<?php if ( have_posts() ) : ?>

		<div class="product-list group">

		<?php WooCommerce\order_product_list(); ?>

		<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<?php echo WooCommerce\reset_previous_product_cat(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php echo WooCommerce\check_if_new_category(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

		</div>

		<?php
			/**
			 * woocommerce_after_shop_loop hook
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		?>

	<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

		<?php wc_get_template( 'loop/no-products-found.php' ); ?>

	<?php endif; ?>

</section>
