<?php
/**
 * The template for displaying the RMA Application page
 *
 * @package WordPress
 * @subpackage Louroe
 */

global $result;
?>

<script>
    $(document).ready(function() {
        $("#rma-submit").click(function() {
            $("#rma-form").submit();
        });

        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#rma-form").submit();
            }
        })
    });
</script>

<div class="rma-form-box">
	<div class="rma-title">LOUROE <span style="color:#3889cb; font-weight:normal">RMA Portal Home.</span></div>

	<div id="rma-listing-div">
		Here is all of the RMA information for your email address, <?=$email?>.
		<?
		foreach ( $result as $item ) {
			?>
			<div class="rma-record-company">
				<div class="column-1"><?=$item['Company']?><br>Contact: <?=$item['ContactName']?></div>
				<div class="column-2">Status: <?=$item['RMAStatus']?></div>
			</div>
			<?php for ($i=1; $i<=3; $i++) {
				$Model = $item["Model{$i}"];
				$Qty = $item["Qty{$i}"];
				$SerialNo = isset($item["SerialNo{$i}"]) ? $item["SerialNo{$i}"] : '';
				if ($Model !='' || $Qty != '' || $SerialNo != '') { ?>
					<div class="rma-record-info">
						<div class="column">Model: <?=$Model?></div>
						<div class="column">Qty: <?=$Qty?></div>
						<div class="column">SerialNo: <?=$SerialNo?></div>
					</div>
				<?php }
			}
			?>
			<div class="rma-record-row">
				<div class="column-1">Problem: <?=$item['Problem']?></div>
			</div>

			<?php if (count($item['rma-details']) != 0) { ?>
				<div class="rma-details">Notes:</div>
				<?php foreach ( $item['rma-details'] as $detailitem ) { ?>
					<div class="rma-details">
						<div class="column-1"><?=$detailitem['CreateDate']?></div>
						<div class="column-2"><?=$detailitem['WebNotes']?></div>
					</div>
				<?php }
			} ?>
			<br><br>



		<?php } ?>


	</div>

</div>
