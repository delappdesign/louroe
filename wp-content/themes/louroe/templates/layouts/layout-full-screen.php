<?php

use Roots\Sage\Wrapper;

/**
 *  Full Screen layout
 *
 *  Triggered when admin selects Full Screen layout for post.
 */

/*
 * Set to true when we want to take advantage of the container class
 * in the theme rather than in the content.
 *
 * i.e. Divi will apply the container class, but archive pages need it in the theme
 */
$use_container = is_archive();

?>

<div class="wrap" role="document">
	<main class="main <?= $use_container ? 'container' : '' ?>">
		<?php include Wrapper\template_path(); ?>
	</main><!-- /.main -->
</div><!-- /.wrap -->