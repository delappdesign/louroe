<?php
/**
 * The template for displaying the distributors page
 *
 * @package WordPress
 * @subpackage Louroe
 */


require 'include/wp_data_inc.php';

$get_variables="action,distance,postalcode,country";
$get_array = explode(",",$get_variables); foreach ($get_array as $value)
{ $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }

//echo "$distance, $postalcode";


if ($postalcode<>'' && $distance<>'')
{


// this sql statement comes from the ultimate locator : location class : function "query()"
	$postal_latitude = "0.0"; $postal_longitude = "0.0";
	$sql_postal = "SELECT * FROM postalcodes where postalcode='$postalcode' ";
//echo $sql_postal;
	$results_zip = mysqli_query($mysqli, "$sql_postal"); if (!$results_zip) { echo 'error in postal'; exit; }
	if($row = mysqli_fetch_assoc($results_zip)) { $postal_latitude = $row['latitude']; $postal_longitude = $row['longitude']; }
//echo $postalcode. " ".$postal_latitude. " ". $postal_longitude;


	$sql = "select d.*, ";
	$sql .= " ( degrees( acos( sin( radians( Latitude ) ) * sin( radians( $postal_latitude ) ) + cos( radians( Latitude ) ) * cos( radians( $postal_latitude ) ) * cos( radians( Longitude - $postal_longitude ) ) ) ) *60 * 1.1515 ) as Distance ";

	$sql .= "from distributors d ";
	$sql .= "WHERE d.Active='Y' and ( ";
	$sql .= "degrees( acos( sin( radians( Latitude ) ) * sin( radians( $postal_latitude ) ) + cos( radians( Latitude ) ) * cos( radians( $postal_latitude ) ) * cos( radians( Longitude - $postal_longitude ) ) ) ) *60 * 1.1515 <= $distance ) ";
	$sql .= " order by Distance ";

//echo '<br>' . $sql;
	$mysqli_result = mysqli_query($mysqli, "$sql"); if (!$mysqli_result) { echo 'error in search distributor'; exit; }
	$distributor_count = mysqli_num_rows($mysqli_result);
	$distributor_result_text = "There are {$distributor_count} Louroe distributors within $distance miles from {$postalcode}.<br><br>";
	if ($distributor_count == 0 ) { $distributor_result_text = "There are no Louroe distributors within $distance miles from $postalcode, please try increasing your search distance.<br><br>"; }
}

else
{

	if ($country=='')
	{
		$sql = "SELECT * FROM distributors where Highlight<>'' and Active='Y' ORDER BY CompanyName ";
		$mysqli_result = mysqli_query($mysqli, "$sql"); if (!$mysqli_result) { echo 'error in highlight distributor'; exit; }
	}
	else
	{
		$stocking='';
		if ($country=='usa') { $sql = "SELECT * FROM distributors where Country='usa' and Active='Y' ORDER BY CompanyName "; }
		if ($country=='canada') { $sql = "SELECT * FROM distributors where Country='canada' and Active='Y' ORDER BY StateOrProvince "; }
		if ($country=='mexico') { $sql = "SELECT * FROM distributors where Country='mexico' and Active='Y' ORDER BY StateOrProvince "; }
		if ($country=='south america') { $sql = "SELECT * FROM distributors where Country='south america' and Active='Y' ORDER BY StateOrProvince "; }
		if ($country=='uk') { $sql = "SELECT * FROM distributors where Country='uk' and Active='Y' ORDER BY StateOrProvince "; }

		$mysqli_result = mysqli_query($mysqli, "$sql"); if (!$mysqli_result) { echo 'error in highlight distributor'; exit; }
	}
}

if ($distance == '') { $distance = '50'; }
?>


<form method="get" action="">
	<center>
		<table border="0" cellpadding="0" cellspacing="0" width="640">
			<?php if ($country == '' or $country = 'USA' ) { ?>
			<tbody><tr>
				<td valign="top" width="175px" class="blueBorder">Postal Code&nbsp;<input type="text" name="postalcode" value="" style="width:75px"></td>
				<td width="10" class="blueBorder"></td>
				<td valign="top" width="175" class="blueBorder">
					Search Distance&nbsp;
					<select size="1" name="distance" id="distance">
						<option value="5">5 Miles</option>
						<option value="10">10 Miles</option>
						<option value="20">20 Miles</option>
						<option value="50" selected="">50 Miles</option>
						<option value="100">100 Miles</option>
						<option value="200">200 Miles</option>
						<option value="300">300 Miles</option>
						<option value="500">500 Miles</option>
					</select>
				</td>
				<td width="10" class="blueBorder"></td>
				<td valign="top" width="175" class="blueBorder"><input type="submit" class="blueBorder" value="Search"></td>
				<td width="10" class="blueBorder"></td>
			</tr>
			<tr>
				<td width=10>&nbsp;</td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="6">
					<div id="country" style="margin-top: 25px">
						<center>
							<?php if ($country=='usa') { echo '<b>USA</b>'; } else { echo "<a href='/about-us/distributors/?country=usa'>USA</a>"; } ?>
							&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
							<?php if ($country=='canada') { echo '<b>CANADA</b>'; } else { echo "<a href='/about-us/distributors/?country=canada'>CANADA</a>"; } ?>
							&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
							<?php if ($country=='mexico') { echo '<b>MEXICO</b>'; } else { echo "<a href='/about-us/distributors/?country=mexico'>MEXICO</a>"; } ?>
							&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
							<?php if ($country=='uk') { echo '<b>UK</b>'; } else { echo "<a href='/about-us/distributors/?country=uk'>UK</a>"; } ?>

					</div>
				</td>
			</tr>

			</tbody></table>
</form>

<br>

<?php	if ($postalcode<>'' && $distance<>'') { echo $distributor_result_text; } ?>

<table border=0 width="640" cellspacing=0 cellpadding="0">
	<tbody>
	<tr>
		<?php
		$cols = -1;

		if (mysqli_num_rows($mysqli_result)<>0) mysqli_data_seek($mysqli_result, 0);
		$currentStateOrProvince = '';
		while($row_1 = mysqli_fetch_assoc($mysqli_result))
		{
			foreach ($row_1 as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };

			$Distance = round( $Distance, 1 );
			$Distance = ( $Distance == '' ) ? '0.0' : number_format($Distance,1);

			$strCompany  = "<b>$CompanyName</b> <br>";
			if ($BillingAddress<>'') { $strCompany .= "$BillingAddress <br>"; }
			$strCompany .= "$City, $StateOrProvince $PostalCode <br>";
			if ($Country<>'') { $strCompany .= "$Country <br>"; }
			if ($PhNumber<>'') { $strCompany .= "Phone: $PhNumber <br>"; }
			if ($AltNumber<>'') { $strCompany .= "Alt: $AltNumber <br>"; }
			if ($Fax<>'') { $strCompany .= "Fax: $Fax <br>"; }
			if ($EmailAddress<>'') { $strCompany .= "Email: <a href='mailto:{$EmailAddress} <br>"; }
			if ($WebsiteAddress<>'') { $strCompany .= "<a class=dist_link href='http://{$WebsiteAddress}' target='blank' >$WebsiteAddress</a> <br>"; }
			if ($Distance<>'' && $Distance > 0) { $strCompany .= "$Distance Miles<br>"; }

			$cols = $cols + 1;
			if ($cols >= 3) $cols = 0;
			if ($cols==0) echo "\r</tr><tr><td>&nbsp;</td></tr><tr>";

			if ( ($currentStateOrProvince <> $StateOrProvince) && ($country=='canada' || $country=='mexico') )
			{
				$currentStateOrProvince = $StateOrProvince;
				echo "\r</tr><tr><td style='color:#943208'><br><b>{$StateOrProvince}</b><br><br></td></tr><tr>";
				$cols = 0;

			}

			?>

			<td valign=top width=175 style='height:140px;'><?=$strCompany?></td>
			<td width=10>&nbsp;</td>

			<?php

		}

		?>

	</tr>




	</tbody></table>
</center></form>