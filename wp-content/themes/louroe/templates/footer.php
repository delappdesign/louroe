<?php

	if (function_exists('get_field')){

		$copyright = get_field('footer_copyright', 'options');

		if ( have_rows('footer_contact_logos', 'options') ){

			$logos = array();
			$contacts = array();

			while( have_rows('footer_contact_logos', 'options') ){

				the_row();

				if ( 'contact_field' === get_row_layout() ){

					$contacts[] = get_sub_field('contact_field');

				} else {

					$logos[] = get_sub_field('logo');

				}

			}

		}

	}

?>

<footer id="page-footer">

	<div id="footer-wrapper" class="container">

			<div class="copyright-nav hidden-md-down">

				<div class="copyright"><?= $copyright ?></div>

				<nav class="nav-footer">
					<?php if (has_nav_menu('footer_navigation')) :
						wp_nav_menu(['theme_location' => 'footer_navigation']);
					endif; ?>
				</nav>

			</div>

			<div class="contact-logos">

				<?php foreach( $contacts as $contact) : ?>
					<div class="contact-logo-item">
						<?= $contact ?>
					</div>
				<?php endforeach; ?>

				<?php foreach( $logos as $logo ): ?>
					<div class="contact-logo-item">
						<img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>"/>
					</div>
				<?php endforeach; ?>

			</div>

		<div class="copyright-nav hidden-lg-up">

			<div class="copyright"><?= $copyright ?></div>

			<nav class="nav-footer">
				<?php if (has_nav_menu('footer_navigation')) :
					wp_nav_menu(['theme_location' => 'footer_navigation']);
				endif; ?>
			</nav>

		</div>

	</div>

</footer>
