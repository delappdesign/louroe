<?php

/**
 * The template for displaying the RMA Application page
 *
 * @package WordPress
 * @subpackage Louroe
 */
?>

<script>
    jQuery(document).ready(function($) {
        $("#rma-submit").click(function() {
            $("#rma-form").submit();
        });

        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#rma-form").submit();
            }
        })
    });
</script>


<div class="rma-form-box">
	<form id="rma-form" name="rma-form" action="" method=post>
		<input type=hidden name=action value=submit>
		<div class="rma-section">
			<div class="column-1-1">
				<div class="row label" title="company">Email</div>
				<div class="row label">RMA Key</div>
			</div>
			<div class="column-1-2">
				<div class="row"><input type=text name=email id=email class="" value="" ></div>
				<div class="row"><input type=text name=rmakey id=rmakey value="" ></div>

			</div>
			<div class="rma-section" >
				<div class="column-3-1">
					<div class="row label">&nbsp;</div>
					<div class="row label"><input id=rma-submit name=rma-submit type=button value="Submit"></div>
					<div class="error"><?=$error?></div>
				</div>
			</div>
		</div>
	</form>
</div>