<?php

if ( function_exists('get_field')){

    // hero image
	$hero_image = get_field('hero_image');
	$image_description = get_field('image_description');
	$hero_cta = [
		'label' => get_field('hero_cta_button_label'),
		'icon' => get_field('hero_cta_button_icon'),
		'url' => get_field('hero_cta_button_url')
	];

	// fast facts
	while ( have_rows('fast_facts') ){

	    the_row();
	    $fast_facts[] = get_sub_field('section');

    }

    // CTA Widgets
    $idx=0;
    while ( have_rows('widgets') ){

        the_row();
        $cta_widgets[] = [
            'image'         => get_sub_field('image'),
	        'caption'       => get_sub_field('caption'),
            'description'   => get_sub_field('description'),
            'button_label'  => get_sub_field('button_label'),
            'button_url'    => get_sub_field('button_url')
        ];

    }

}

?>

<?php if ( ! empty( $hero_image ) ) : ?>
<style>
	.hero-image{
		background-image: url("<?= $hero_image['sizes']['medium_large'] ?>");
		/*height: <?= $hero_image['sizes']['medium_large-height'] ?>px;*/
	}
	@media screen and (min-width: 768px) {
		.hero-image{
			background-image: url("<?= $hero_image['sizes']['large'] ?>");
			/*height: <?= $hero_image['sizes']['large-height'] ?>px;*/
		}
	}

	@media screen and (min-width: 1024px) {
		.hero-image{
			background-image: url("<?= $hero_image['url'] ?>");
			/*height: <?= $hero_image['height'] ?>px;*/
		}
	}
</style>
<section class="hero-image">

	<div class="container">

		<div class="description-wrapper">
			<div class="description"><?= $image_description;?></div>

			<a class="btn" href="<?= $hero_cta['url']; ?>"><?= $hero_cta['label'] ?>
				<?php if( !empty($hero_cta['icon']) ):?>
					<img src="<?= $hero_cta['icon']['url']; ?>">
				<?php endif; ?>
			</a>

		</div>

	</div>
</section>
<?php endif; ?>

<?php if( ! empty( $fast_facts ) ): ?>
<section class="fast-facts">

    <div class="container">

        <div class="row">

            <?php foreach( $fast_facts as $fast_fact): ?>

                <div class="fast-fact col-xs-12 col-sm"><?= $fast_fact ?></div>

            <?php endforeach; ?>

        </div>

    </div>


</section>
<div class="border-wrapper">
    <div class="ff-border col-xs"></div>
    <div class="ff-border col-xs"></div>
    <div class="ff-border col-xs"></div>
</div>
<?php endif; ?>


<?php if( ! empty( $cta_widgets ) ): ?>
<section class="cta-widgets">

    <div class="container">

        <div class="row">

			<?php foreach( $cta_widgets as $cta_widget): ?>

                <div class="cta-widget col-xs-12 col-md-4">

                    <div class="image-wrapper">
                        <img src="<?= $cta_widget['image']['url'] ?>" alt="<?= $cta_widget['image']['alt'] ?>">
                        <div class="caption"><?= $cta_widget['caption'] ?></div>
                    </div>

                    <div class="description"><?= $cta_widget['description'] ?></div>

                    <div class="button-wrapper">
                        <a class="btn" href="<?= $cta_widget['button_url'] ?>"><?= $cta_widget['button_label'] ?></a>
                    </div>


                </div>

			<?php endforeach; ?>

        </div>

    </div>


</section>
<?php endif; ?>


