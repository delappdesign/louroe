<?php
/**
 * The template for displaying the RMA Application page
 *
 * @package WordPress
 * @subpackage Louroe
 */
?>


<div class="rma-description" style="">
	<p>Repairs and returns are now easier.  Simply fill out the online form below to submit your RMA application.</p><p>Please allow 2 business days to begin processing.  You can login to the <a href="/support/rma-portal">RMA Portal here</a> upon receipt of your confirmation email.</p>
	<p>If you need assistance please contact us via email at <a href="mailto:rma@louroe.com">rma@louroe.com</a> or by phone at 818.994.6498 x224.
	<p>&nbsp;</p>
	<p></p>
</div>

<div style="display:block; border-top: 1px solid #3889cf; padding:20px 0 20px 0; "></div>
<div class="rma-form-box">
	<form id="rma-form" name="rma-form" action="" method=post>
		<input type=hidden name=action value=submit>


		<div class="rma-section">
			<div class="column-1-1">
				<div class="row label-required" title="company">Company</div>
				<div class="row label-required" title="address1">Address</div>
				<div class="row label">&nbsp;</div>
				<div class="row label-required" title="city">City</div>
				<div class="row label-required" title="state">State</div>
				<div class="row label-required" title="zip">Zip</div>
				<div class="row label">&nbsp;</div>
				<div class="row label-required" title="contactName">Contact Name</div>
				<div class="row label-required" title="phone">Phone</div>
				<div class="row label">Fax</div>
				<div class="row label-required" title="email">Email</div>
			</div>
			<div class="column-1-2">
				<div class="row "><input type=text name=company id=company class="required" value="" tabindex=100 ></div>
				<div class="row"><input type=text name=address1 id=address1 class="required" value="" tabindex=101 ></div>
				<div class="row"><input type=text name=address2 id=address2 value="" tabindex=102 ></div>
				<div class="row"><input type=text name=city id=city class="required"value="" tabindex=102 ></div>
				<div class="row"><input type=text name=state id=state class="required"value="" tabindex=102 ></div>
				<div class="row"><input type=text name=zip id=zip class="required" value="" tabindex=102 ></div>
				<div class="row">&nbsp;</div>
				<div class="row"><input type=text name=contactName id=contactName class="required" value="" tabindex=103 ></div>
				<div class="row"><input type=text name=phone id=phone class="required" value="" tabindex=104 ></div>
				<div class="row"><input type=text name=fax id=fax value="" tabindex=105 ></div>
				<div class="row"><input type=text name=email id=email class="required" value="" tabindex=106 ></div>
			</div>
		</div>

		<div id="rma-products" class="rma-section" style="">
			<div class="column-2-1">
				<div id="modellabel_1" class="row label-required" title="model1" >Model</div>
				<div id="modellabel_2"  class="row label">Model</div>
				<div id="modellabel_3"  class="row label">Model</div>
				<div id="modellabel_4"  class="row label">Model</div>
				<div id="modellabel_5"  class="row label">Model</div>
			</div>
			<div class="column-2-2">
				<div class="row label"><input type=text name=model1 id=model1 class="required" value="" tabindex=200 ></div>
				<div class="row label"><input type=text name=model2 id=model2 class="model" value="" tabindex=205 ></div>
				<div class="row label"><input type=text name=model3 id=model3 class="model" value="" tabindex=210 ></div>
				<div class="row label"><input type=text name=model3 id=model4 class="model" value="" tabindex=215 ></div>
				<div class="row label"><input type=text name=model3 id=model5 class="model" value="" tabindex=220 ></div>
			</div>
			<div class="column-2-1">
				<div id="qtylabel_1" class="row label-required">Qty</div>
				<div id="qtylabel_2" class="row label">Qty</div>
				<div id="qtylabel_3" class="row label">Qty</div>
				<div id="qtylabel_4" class="row label">Qty</div>
				<div id="qtylabel_5" class="row label">Qty</div>
			</div>
			<div class="column-2-2">
				<div class="row label-required"><input type=text name=qty1 id=qty1 class="required" value="" tabindex=201 ></div>
				<div class="row label"><input type=text name=qty2 id=qty2 value="" tabindex=206 ></div>
				<div class="row label"><input type=text name=qty3 id=qty3 value="" tabindex=211 ></div>
				<div class="row label"><input type=text name=qty3 id=qty4 value="" tabindex=216 ></div>
				<div class="row label"><input type=text name=qty3 id=qty5 value="" tabindex=221 ></div>
			</div>
			<div class="column-2-1">
				<div id="snlabel_1" class="row label">Serial #</div>
				<div id="snlabel_2" class="row label">Serial #</div>
				<div id="snlabel_3" class="row label">Serial #</div>
				<div id="snlabel_4" class="row label">Serial #</div>
				<div id="snlabel_5" class="row label">Serial #</div>
			</div>
			<div class="column-2-2">
				<div class="row label"><input type=text name=sn1 id=sn1 value="" tabindex=202 ></div>
				<div class="row label"><input type=text name=sn2 id=sn2 value="" tabindex=207 ></div>
				<div class="row label"><input type=text name=sn3 id=sn3 value="" tabindex=212 ></div>
				<div class="row label"><input type=text name=sn3 id=sn4 value="" tabindex=217 ></div>
				<div class="row label"><input type=text name=sn3 id=sn5 value="" tabindex=222 ></div>
			</div>
			<div class="column-2-3">
				<div class="row label">&nbsp;<div id="warrantylabel_1" style="display:inline-block;" ><input type=checkbox name=warranty1 id=warranty1 class="warranty" tabindex=203>Warranty</div></div>
				<div class="row label">&nbsp;<div id="warrantylabel_2" style="display:inline-block;" ><input type=checkbox name=warranty2 id=warranty2 class="warranty" tabindex=208>Warranty</div></div>
				<div class="row label">&nbsp;<div id="warrantylabel_3" style="display:inline-block;" ><input type=checkbox name=warranty3 id=warranty3 class="warranty" tabindex=213>Warranty</div></div>
				<div class="row label">&nbsp;<div id="warrantylabel_4" style="display:inline-block;" ><input type=checkbox name=warranty4 id=warranty4 class="warranty" tabindex=218>Warranty</div></div>
				<div class="row label">&nbsp;<div id="warrantylabel_5" style="display:inline-block;" ><input type=checkbox name=warranty5 id=warranty5 class="warranty" tabindex=223>Warranty</div></div>
			</div>
		</div>

		<div class="rma-section" style="margin-top:10px; ">
			<div class="column-2-1">
				<div class="row label-required" title="problem">Problem</div>
			</div>
			<div class="column-2-2">
				<div class="row label"><textarea name=problem id=problem class="required" tabindex=300 ></textarea></div>
			</div>
		</div>

		<div class="rma-section" style="">
			<div class="column-3-1">
				<div class="row label label-required" title="request"><span id=request>Request</span></div>
				<div class="row label"><input type=radio name=repairReturn tabindex=301 >&nbsp;Repair</div>
				<div class="row label"><input type=radio name=repairReturn tabindex=302 >&nbsp;Return for Credit</div>
			</div>
		</div>

		<div class="rma-section" >
			<div class="column-3-1">
				<div id="form-error" class="row label-required">Required Fields</div>
				<div class="row label"><input id=rma-submit-button name=rma-submit-button type=button value="Submit RMA Application" tabindex=303></div>
			</div>
		</div>

	</form>
</div>