<?php  use Roots\Sage\Assets; ?>
<!-- Slider Section -->
<?php
	$gallery = get_field('le_banner_gallery');

	if( $gallery ): ?>
		<section class="banner-gallery flexslider">
			
			<ul class="slides">
				<?php foreach( $gallery as $slide ): ?>
					<li class="imgid-<?php echo $slide['id']; ?>">
						<img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" />
						 <!-- <img  
							srcset="<?php //echo $slide['url']; ?> 1920w,
									<?php //echo $slide['sizes']['xlarge']; ?> 1170w,
									<?php //echo $slide['sizes']['large']; ?> 960w,
									<?php //echo $slide['sizes']['medium']; ?> 768w,
									<?php //echo $slide['sizes']['small']; ?> 470w"
							src="<?php //echo $slide['sizes']['medium']; ?>"
							sizes="100vw"
							alt="<?php //echo $slide['alt']; ?>" /> -->
						<div class="caption-wrap container">
							<div class="caption">
								<h2><?php echo $slide['caption']; ?></h2>
								<div><?php echo $slide['description']; ?></div>
								<a class="btn btn-primary" href="<?php echo esc_html (get_field('le_image_link', $slide['ID'])); ?>"><?php echo esc_html (get_field('le_slider_button_text', $slide['ID'])); ?></a>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="controls"></div>
		</section>
<?php
		endif;
	?>

<!-- Top Products Section -->

<section class="home-products">
	<div class="container">
		<?php if ( get_field('le_top_products_header') ) : ?>
			<h2><?php echo esc_html (get_field('le_top_products_header')); ?></h2>
		<?php endif;
		// check if the Top Products repeater field has rows of data
			if( have_rows('le_top_products') ): ?>
				<div class="top-products">
					<ul class="slides">
					<?php // loop through the rows of data
						while ( have_rows('le_top_products') ) : the_row(); 
							$product = get_sub_field('product'); 
							if ( $product ) : 
								// override $post
								$post = $product;
								setup_postdata( $post ); ?>
								<li class="product">
									<?php if ( has_post_thumbnail() ) : ?>
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php the_post_thumbnail('thumbnail'); ?>
										</a>
									<?php endif; ?>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>	
									<p><?php //the_excerpt(); ?></p>	
								</li>
							<?php wp_reset_postdata();
							 endif; ?>
					<?php endwhile; ?>
					</ul>
				</div>
			<?php else :

			  // no rows found
			
			endif; ?> 
		<?php 
		// check if the Product Links repeater field has rows of data
			if( have_rows('le_product_links') ): ?>
					<ul class="product-links">
					<?php // loop through the rows of data
						while ( have_rows('le_product_links') ) : the_row(); ?>
							<li>
								<a href="<?php echo esc_html (get_sub_field('link_url')); ?>">
									<?php echo esc_html (get_sub_field('link_text')); ?>
									<img src="<?= Assets\asset_path('images/icon-arrow-blue.png') ?>" />
								</a>
							</li>
					<?php endwhile; ?>
					</ul>
			<?php else :
			  // no rows found
			endif; ?> 	
	</div>
</section>

<!-- Solutions Section -->

<section class="home-solutions">
	<div class="container">
		
		<?php // check if the Solutions repeater field has rows of data
		if (have_rows('le_solutions')) : 
		  // Nav Tabs 
			$optionCounter = 0; ?>
		<div id="solutionTabs">
			<div class="navigation">
				<?php if ( get_field('le_solutions_header') ) : ?>
					<h2><?php echo esc_html (get_field('le_solutions_header')); ?></h2>
				<?php endif; ?>
			
				<form>
					<select id='tabSelect'>
						<?php while(have_rows('le_solutions')) : the_row(); ?>
							<option value='<?php echo $optionCounter; ?>'><?php echo esc_html (get_sub_field('tab_text')); ?></option>
						<?php $optionCounter ++;
						endwhile; ?>
					</select>
				</form>
				<?php // reset the rows of the repeater
			 		reset_rows(); 
			 		$tabCounter = 1;?>
				<ul class="nav nav-tabs" role="tablist" id="tabNav">
					<?php while(have_rows('le_solutions')) : the_row();
						$icon = get_sub_field('tab_icon'); 
						if ($tabCounter == 1 ) :
							$class = 'class="active"'; 
						else :
							$class = '';
						endif; ?> 
						<li role="presentation" class="nav-item"><a href="#tab<?php echo $tabCounter; ?>" <?php echo $class; ?> aria-controls="tab<?php echo $tabCounter; ?>" role="tab" data-toggle="tab">
							<?php if (!empty($icon)) : ?>
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" /><?php echo esc_html (get_sub_field('tab_text')); ?>
							<?php endif; ?> 
							</a>
						</li>
						<?php $tabCounter ++;
					endwhile; ?>
				</ul> <!-- end .nav-tabs -->
			</div>
		<?php // reset the rows of the repeater
		 	reset_rows();
		  
		 	// Tabs 
		 	$divCounter = 1;?>
			<div class="tab-content">
			<?php while(have_rows('le_solutions')) : the_row(); 
			$embedcode = get_sub_field('youtube_id');
			if ($divCounter == 1 ) :
					$class = ' active'; 
				else :
					$class = '';
				endif; ?> 
				<div role="tabpanel" class="tab-pane<?php echo $class; ?>" id="tab<?php echo $divCounter; ?>">
					<div>
						<?php if (!empty($embedcode)) : ?> 
							<div class="video-container">
								<iframe src="https://www.youtube.com/embed/<?php echo $embedcode; ?>" width="600" height="350" frameborder="0"></iframe>
							</div>
						<?php endif; ?>
						<?php if ( get_sub_field('description') || get_sub_field('button_link')) : ?>
							<div class="video-text">
								<div class="content-wrapper">
						<?php endif; ?>
							<?php if ( get_sub_field('description') ) : ?>
								<p class="description">
									<?php echo esc_html (get_sub_field('description')); ?>
								</p>
							<?php endif; ?>
							<?php if ( get_sub_field('button_link') ) : ?>
								<a class="btn btn-primary" href="<?php echo esc_html (get_sub_field('button_link')); ?>"><?php echo esc_html (get_sub_field('button_text')); ?></a>
							<?php endif; ?>
						<?php if ( get_sub_field('description') || get_sub_field('button_link')) : ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			
			<?php 
			$divCounter ++;
			endwhile; ?>
			</div><!-- end .tab-content -->
		<?php else :
			  // no rows found
			endif; ?> 
		</div><!-- end #solutionTabs -->	
	</div><!-- end .container -->
</section><!-- end .home-solutions -->

<!-- Case Studies-Support Section -->

<section class="home-case-studies-support">
	<div class="container">
<!-- Case Studies -->
		<div class="case-studies">
			<?php if ( get_field('le_case_studies_header') ) : ?>
				<h2><?php echo esc_html (get_field('le_case_studies_header')); ?></h2>
			<?php endif;

			$image = get_field('le_case_studies_image'); 
			if( !empty($image) ): ?>

				<img class="img-fluid" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif;
			 // check if the Case Studies repeater field has rows of data
			if (have_rows('le_case_studies')) : 
			  // Case Studies Links ?>
				<ul>
					<?php while(have_rows('le_case_studies')) : the_row(); 
						$caseStudy = get_sub_field('case_study');
						if ( $caseStudy ) : 
							// override $post
							$post = $caseStudy;
							setup_postdata( $post ); ?>
							<li>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</li>
						<?php wp_reset_postdata();
						 endif; 
						 endwhile; ?>
				</ul> 
			<?php else :
				// no rows found
			endif; ?>
			<?php if ( function_exists('get_field') && get_field('cs_view_more_url')):?>
				<a class="btn btn-primary" href="<?=get_field('cs_view_more_url')?>"><?= get_field('cs_view_more_label')?></a>
			<?php endif; ?>
		</div><!-- end .case-studies -->
<!-- Support -->
		<div class="support">
			<?php if ( get_field('le_support_center_header') ) : ?>
				<h2><?php echo esc_html (get_field('le_support_center_header')); ?></h2>
			<?php endif;
			$image = get_field('le_support_image'); 
			if (!empty($image)): ?>
				<img class="img-fluid" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
			<?php if ( get_field('le_support_sub_header') ) : ?>
				<h3><?php echo esc_html (get_field('le_support_sub_header')); ?></h3>
			<?php endif; ?>
			<div class="support-text">
				<?php if ( get_field('le_support_text') ) : ?>
					<div><?php echo wp_kses_post (get_field('le_support_text')); ?></div>
				<?php endif; ?>
				<?php 
				// check if the Support Links repeater field has rows of data
					if( have_rows('le_support_links') ): ?>
							<ul class="support-links">
							<?php // loop through the rows of data
								while ( have_rows('le_support_links') ) : the_row(); ?>
									<li>
										<a href="<?php echo esc_html (get_sub_field('link_url')); ?>">
											<?php echo esc_html (get_sub_field('link_text')); ?>
										</a>
									</li>
							<?php endwhile; ?>
							</ul>
					<?php else :
					  // no rows found
					endif; ?> 
			</div>
		</div><!-- end .support -->
	</div><!-- end .container -->
</section><!-- end .home-case-studies-support -->