<?php
/**
 * The template for displaying the RMA Application page
 *
 * @package WordPress
 * @subpackage Louroe
 */
?>

<div class="rma-form-containter">
	<div class="rma-form-box">
		<div class="label">Thank you for submitting your LOUROE <span style="color:#3889cb; font-weight:normal">Online RMA Application</span>.&nbsp;&nbsp;A confirmation has been sent to your email address.</div>
	</div>
</div>

<div class="rma-description">
	<p><br><br>Please allow 2 business days to begin processing.</p>
	<p>If you need assistance please contact us via email at <a href="mailto:rma@louroe.com">rma@louroe.com</a> or by phone at 818.994.6498 x211.
</div>
