<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div class="product-sidebar">
	<h2 class="title">Browse All Products</h2>
  <ul>
  	<?php dynamic_sidebar('product-sidebar'); ?>
  </ul>
</div>

<div id="product-container" itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>

	

		<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div><!-- .summary -->
	<meta itemprop="url" content="<?php the_permalink(); ?>" />


<?php global $post, $woocommerce, $product; ?>
<div class="product-sidebar-right">

	<?php //get_template_part('templates/sections/product', 'images'); ?>

	<?php wc_get_template( 'single-product/product-image.php' );  ?>

	<div class="desktop ctas-extended-info">
		<?php get_template_part('templates/components/product', 'ctas-extended-info'); ?>
	</div>

</div><!-- end .product-sidebar-right -->
</div><!-- #product-<?php the_ID(); ?> -->



<!-- </div> --><!-- end hero -->


<?php do_action( 'woocommerce_after_single_product' ); ?>

<script type="text/javascript">
  jQuery('.sidebar-tab li a[href="#"]').on('click', function(e) {
    e.preventDefault();
    var toggleText = jQuery(this).find('.sidebar-icon');

    if (toggleText.text() == "+") {
        toggleText.text('-');
    } else {
        toggleText.text('+');
    }

    jQuery(this).next('.sidebar-div').toggle();
})

</script>