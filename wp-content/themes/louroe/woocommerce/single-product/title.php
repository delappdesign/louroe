<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
<span class="product-number">PRODUCT NUMBER: <?php echo get_field('product_number'); ?></span>

<?php //get_template_part('templates/sections/product', 'images'); ?>

<?php wc_get_template( 'single-product/product-image.php' );  ?>
