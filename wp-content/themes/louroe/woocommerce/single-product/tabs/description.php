<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Product Description', 'woocommerce' ) ) );
?>

<?php if ( $heading ): ?>
  <!-- <h2><?php echo $heading; ?></h2> -->
<?php endif; ?>

<?php the_content(); ?>

<div class="mobile ctas-extended-info">
	<?php get_template_part('templates/components/product', 'ctas-extended-info'); ?>
</div>

<hr />
<h4><strong>Where to Use</strong></h4>
<div class="group">
<?php
$uses = get_field('product_uses');
if($uses) {
foreach($uses as $use) {
  $n_use = strtolower($use['icon']);
  $n_use = preg_replace("/[\s_]/", "-", $n_use);
  echo '<div class="icon-container fl"><a href="',$use['link'],'"><div class="icon ',$n_use,'"></div><span>',$use['icon'],'</span></a></div>';
}
}
?>
</div>
<?php $awards = get_field('product_certifications');
if($awards):?>
<hr />
<h4><strong>Certification/Awards</strong></h4>
<div class="group">
<?php
foreach($awards as $award) {
  echo '<img class="awards" src="',$award["image"],'"/>';
}
?>
</div>
<?php endif; ?>