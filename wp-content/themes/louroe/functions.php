<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
	'lib/assets.php',                                 // Scripts and stylesheets
	'lib/extras.php',                                 // Custom functions
	'lib/setup.php',                                  // Theme setup
	'lib/titles.php',                                 // Page titles
	'lib/wrapper.php',                                // Theme wrapper class
	'lib/customizer.php',                             // Theme customizer
	'lib/nav-walker-bootstrap.php',                   // Navwalker used by mobile menu
	'lib/nav-walker-mega-menu.php',                   // Navwalker used by mobile menu
	'lib/nav-walker-bootstrap-nlevel.php',            // Navwalker used by main menu
	'lib/detect-browser-features.php',                // Add classes to body tag for browser clients
	'lib/woocommerce.php',                            // add custom functions to support woocommerce
	'lib/rma-application-handler.php',                // handle RMA applications
	'lib/rma-portal-login-handler.php',               // handle RMA portal logins
	'lib/rma-portal-main-handler.php',                // handle RMA portal logins
	'lib/class-distributor-geolocation-handler.php'   // handle geolocation and finding distributor info
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
