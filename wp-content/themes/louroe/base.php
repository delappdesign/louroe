<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

global $is_full_screen;

/*
 * set is_full_screen when custom field is set, or for the following:
 * - all archive pages (woocommerce)
 * - WooCommerce product pages
 */
$is_full_screen = ( function_exists( 'get_field' ) && 'full_screen' === get_field('lou_layout') ) ||
                  ( is_archive() &&
                    ! is_post_type_archive(['tribe_events']) ) ||
                  is_product();

$shows_featured_image =  ! (is_search() ||
                           ( is_archive() &&
                             ! is_post_type_archive(['tribe_events']) ) ||
                           is_front_page() ||
                           is_product()  );

if ( function_exists('get_field') ){
	$show_slide_out_tab = get_field('show_slide-out_tab', 'options');
}
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

    <?php if ( $shows_featured_image & has_post_thumbnail()): ?>
      <div id="hero" class="container">
        <?php the_post_thumbnail() ?>
      </div>
    <?php endif; ?>
    
    <?php if ( $is_full_screen ) {

	    get_template_part( 'templates/layouts/layout', 'full-screen' );

    } else {

	     get_template_part( 'templates/layouts/layout', 'right-sidebar' );

    }?>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>

    <?php if ($show_slide_out_tab){
	    get_template_part( 'templates/components/tab', 'slide-out' );
    }?>
	<script type="text/javascript">
_linkedin_data_partner_id = "76130";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=76130&fmt=gif" />
</noscript>
  </body>
</html>
