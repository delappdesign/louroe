<?php while (have_posts()) : the_post(); ?>
	<div class="container">
		<?php get_template_part('templates/page', 'header'); ?>
		<?php get_template_part('templates/content', 'rma-portal-main'); ?>
	</div>
<?php endwhile; ?>