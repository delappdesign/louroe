jQuery(document).ready(function($) {

	$("#rma-submit-button").click(function() {

		// clear all fields
		$(".label-required").removeClass("error");
		$("#form-error").html('Required Fields');
		$("#form-error").removeClass("error");
		
		// determine required fields
		var error = false;
		$('.required').each(function() {
			field = this.id;
			value = $("#"+field).val();
			
			// skip elements that are part of the product list
			if ( $.contains(  document.getElementById("rma-products"), this  )) { return true; }

			if ($("#"+field).val()=='') {
				error = true;
				$('div[title="'+field+'"]').addClass("error");
			}
			
			if (field=='phone') { 
				phoneReg = /[0-9 -()+]+$/;
				if(!phoneReg.test(value)) { error=true; $('div[title="'+field+'"]').addClass("error");  }
			}
			
			if (field=='email') { 
				emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(value)) { error=true; $('div[title="'+field+'"]').addClass("error");  }
			}
		});
		
		//return;
		
		// check the request
		if ($("input:radio[name=repairReturn]:checked").length == 0) {
		//if (!$('#repairReturn').attr('checked') && !$('#repair').attr('checked')) {
			error = true;
			$('div[title="request"]').addClass("error");
		}

		// check the items
		for (i=1; i<=5; i++) {
		
			if ( $('#model'+i).hasClass('required') && 
				( $('#model'+i).val()=='' || $('#qty'+i).val()=='' || ( $('#sn'+i).hasClass('required') && $('#sn'+i).val()=='' ) )
			) {
				if ($('#model'+i).val()=='') { $('#modellabel_'+i).addClass("error"); }
				if ($('#qty'+i).val()=='') { $('#qtylabel_'+i).addClass("error"); }
				if ( $('#sn'+i).hasClass('required') && $('#sn'+i).val()=='' ) { $('#snlabel_'+i).addClass("error"); }
				error = true;
			} else {
				$('#modellabel_'+i).removeClass('error');
				$('#qtylabel_'+i).removeClass('error');
				$('#snlabel'+i).removeClass("error");
			}
		}
		

		//error = false
		if (error) {

			$("#form-error").html('Please review the required fields.');
			$("#form-error").addClass("error");
			return false;
		} else {
			//document.getElementById('rma-form').submit();
			$("#rma-submit-button").prop("value","Processing..");
			$("#rma-submit-button").prop("disabled", true);
			$('#rma-form').submit();
		}

	});
	
	$(".warranty").click(function() {
		id = this.id;
		num = id.charAt( id.length-1 );
		if (this.checked) { 
			$('#snlabel_'+num).removeClass('label').addClass('label-required'); 
			$('#sn'+num).addClass('required');
		} else { 
			$('#snlabel_'+num).removeClass('label-required').addClass('label'); 
			$('#sn'+num).removeClass('required');
			$('#snlabel_'+num).removeClass('error');
		}
	});
	
	// hide the warranty for blank model field
	for (i=2; i<=5; i++) {
		$('#warrantylabel_'+i).hide();
	}
	
	$( ".model" ).keyup(function() {
		id = this.id;
		num = id.charAt( id.length-1 );
		if ($(this).val()!='') {
			$('#warrantylabel_'+num).show();
			$('#modellabel_'+num).removeClass('label').addClass('label-required');
			$('#qtylabel_'+num).removeClass('label').addClass('label-required');
			$('#model'+num).addClass('required');
		} else {
			$('#warrantylabel_'+num).hide();
			$('#snlabel_'+num).removeClass('label-required').addClass('label'); 
			$('#modellabel_'+num).removeClass('label-required').addClass('label');
			$('#qtylabel_'+num).removeClass('label-required').addClass('label'); 
			$('#sn'+num).removeClass('required');
			$('#warranty'+num).prop('checked',false);
			$('#model'+num).removeClass('required');
		}
	});
	
	$.fn.qtip.styles.mystyle = { // Last part is the name of the style
	   width: 200,
	   padding: '0 5px 0 5px', background: '#e8c1a0', color: 'black', 'font-size': '9pt', 'line-height':'22px', textAlign: 'center',
	   border: { width: '1px', radius: 5, color: '#e8c1a0' },
	   tip: 'bottomLeft'
	}	

	$('#request').qtip({
   		content: 'Certain charges may apply for repair and restocking',
   		show: 'mouseover', hide: 'mouseout', style: 'mystyle',
   		position: {
      		corner: { target: 'topRight', tooltip: 'bottomLeft' }
   		}
	});
	
	$('#warrantylabel_1').qtip({
   		content: 'Warranty...',
   		show: 'mouseover', hide: 'mouseout', style: 'mystyle',
   		position: {
      		corner: { target: 'topRight', tooltip: 'bottomLeft' }
   		}
	});
	
	$('#company').focus();

});