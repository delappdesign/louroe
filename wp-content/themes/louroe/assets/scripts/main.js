/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {

                /******************************************************
                 *  Menu Behaviors
                 */
                var $dropdown = $('.dropdown');

                /*
                 * Add slide up/down behavior to submenus
                 */
                $dropdown.on('show.bs.dropdown', function (e) {
                    $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
                });
                $dropdown.on('hide.bs.dropdown', function (e) {
                    $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
                });

                /*
                 * Mobile side navigation behaviors
                 */
                var $sideNavOpenTrigger = $('.navbar-toggler.opener'),
                    $sideNavCloseTriggers = $('header, footer, body > .wrap, .navbar-toggler.closer'),
                    $sideNav = $('.sidenav');

                // Close mobile side navigation
                $sideNavCloseTriggers.click(function () {
                    $sideNav.css({
                        right: '-300px',
                        opacity: 0
                    });
                });

                // Open mobile side navigation
                $sideNavOpenTrigger.click(function (e) {
                    $sideNav.css({
                        right: 0,
                        opacity: 1
                    });

                    // Stop the close nav behavior from triggering when clicking the open nav button.
                    e.stopImmediatePropagation();
                });

                /*
                 *  Open submenu on hover
                 */
                $desktopDropdown = $('.desktop .dropdown');
                $desktopDropdown.hoverIntent({
                    sensitivity: 1,
                    interval: 10,
                    timeout: 100,
                    over: function () {
                        $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
                    },
                    out: function () {
                        $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
                    }
                });

                //make dropdown parent link work as link, not just toggle
                /*$('.mobile-nav .dropdown-toggle').click(function( e ) {
                    $this = $(this);
                    if ( $this.parent('.open').length ) {
                        e.stopPropagation();
                        var location = $this.attr('href');
                        window.location.href = location;
                        return false;
                    }
                });*/

                /******************************************************
                 * Mega Menu Behaviors
                 */
                var setMegaMenuWidth = function () {

                    var $menu = $('nav.nav-primary:not(.mobile-nav)'),
                        menuWidth = $menu.width(),
                        $mmItems = $menu.find('.mega-menu');

                    /*
                     * set position and width of any mega menus,
                     * being mindful of where it lies in relation to the screen edge
                     */
                    $mmItems.each(function () {
                        var $mmItem = $(this),
                            $mmList = $mmItem.find('>ul'),
                            itemOffset = $mmItem.offset().left,
                            listOffset = (-menuWidth / 2 + $mmItem.width() / 2),
                            docWidth = $(document).width();

                        // set mega menu width to same as whole menu by default
                        $mmList.css('width', menuWidth);

                        // adjust mega menu position so it doesn't go of edge of screen
                        if (10 > itemOffset + listOffset) {
                            listOffset -= itemOffset - menuWidth / 2;
                        } else if ((docWidth - 10) < itemOffset + $mmItem.width() / 2 + menuWidth / 2) {
                            listOffset -= docWidth - (itemOffset + $mmItem.width() / 2 + menuWidth / 2) + 10;
                        }

                        $mmList.css('left', listOffset);

                    });

                };

                setMegaMenuWidth();

                $(window).resize(setMegaMenuWidth);

                /******************************************************
                 *  Toggle mobile search form
                 */
                var $searchPanel = $('.search-panel'),
                    $searchOpenTrigger = $('.search-btn'),
                    $searchCloseTriggers = $('.search-panel .close-btn');

                // Close mobile side navigation
                $searchCloseTriggers.click(function () {
                    $searchPanel.css({
                        top: '-85px',
                        opacity: 0
                    });
                });

                // Open mobile side navigation
                $searchOpenTrigger.click(function (e) {
                    $searchPanel.css({
                        top: '55px',
                        opacity: 1
                    });

                    // Stop the close nav behavior from triggering when clicking the open nav button.
                    e.stopImmediatePropagation();
                });


                /*****************************************************************
                 * Slide-out Tab behavior
                 */
                var $slideOutTab = $('#slide-out-tab-wrapper'),
                    $slideOutContent = $slideOutTab.find('.slide-out-tab-content'),
                    $slideOutTitle = $slideOutTab.find('.title'),
                    $slideOutOpener = $slideOutTab.find('.opener'),
                    $slideOutCloser = $slideOutTab.find('.closer'),
                    $slideOutError = $slideOutTab.find('.validation_error'),
                    sliderTitleHeight = $slideOutTab.find('.title').outerHeight(), // 46
                    $window = $(window),
                    isPreviouslyDismissed = ('true' === Cookies.get('SlideOutTabDismissed')),
                    isInitiallyClosed = ! $slideOutError.is(':visible') && ($window.width() <= 991 || isPreviouslyDismissed),

                    // correct for scrollbars on some browsers
                    widenOnScroll = function () {
                        // add to width if is ie and has vertical scrollbar
                        var ua = window.navigator.userAgent,
                            isIE = ua.indexOf('Edge/') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf("MSIE ") > 0,
                            isScrollbar = $slideOutContent.get(0).scrollHeight > $slideOutContent.height();
                        if (isIE && isScrollbar) {
                            $slideOutTab.width($slideOutTab.width() + 10);
                        }
                    },


                    /*
                     * If slide-out is initially closed, we need make bottom: auto, and top calculated
                     * Otherwise, if slide-out is showing initially, we need top: auto, and bottom calculated
                     * This is because the content is populated asynchronously via hubspot
                     */
                    openTab = function (animate) {
                        if (isInitiallyClosed) {
                            var tabTop = {top: $window.height() - $slideOutTab.height() + 'px'};

                            $slideOutTab.css('bottom', 'auto');
                            if (animate) {
                                $slideOutTab.animate(tabTop);
                            } else {
                                $slideOutTab.css(tabTop)
                            }
                        } else {
                            var tabBottom = {bottom: 0};

                            $slideOutTab.css('top', 'auto');
                            $slideOutTab.animate();
                            if (animate) {
                                $slideOutTab.animate(tabBottom);
                            } else {
                                $slideOutTab.css(tabBottom)
                            }
                        }

                        $slideOutOpener.hide();
                        $slideOutCloser.show();
                    },

                    closeTab = function (animate) {
                        if (isInitiallyClosed) {
                            var tabTop = {top: $window.height() - sliderTitleHeight + 'px'};

                            $slideOutTab.css('bottom', 'auto');
                            if (animate) {
                                $slideOutTab.animate(tabTop);
                            } else {
                                $slideOutTab.css(tabTop)
                            }
                        } else {
                            var tabBottom = {bottom: -$slideOutTab.height() + sliderTitleHeight + 'px'};

                            $slideOutTab.css('top', 'auto');
                            $slideOutTab.animate();
                            if (animate) {
                                $slideOutTab.animate(tabBottom);
                            } else {
                                $slideOutTab.css(tabBottom)
                            }
                        }

                        $slideOutOpener.show();
                        $slideOutCloser.hide();
                    },

                    isClosed = function () {
                        return $slideOutOpener.is(':visible');
                    },

                    updateTabPosition = function () {
                        if (isClosed()) {
                            closeTab();
                        } else {
                            openTab();
                        }
                    };


                /*
                 * -- Initial state --
                 * open if validation error, or desktop width and not previously dismissed
                 * close if mobile width or previously dismissed
                 */
                if (isInitiallyClosed) {
                    closeTab();
                } else {
                    openTab();
                    widenOnScroll();
                }

                // Toggle Slide-out tab
                $slideOutTitle.click(function (e) {

                    if (isClosed()) {
                        openTab(true);
                    } else {
                        closeTab(true);

                        /*
                         * if not previously dismissed (on a previous page load), set a cookie to remember this was
                         * dismissed.
                         * Don't update the value of isPreviouslyDismissed because we need to track initial state on
                         * page load.
                         * Set cookie expiration for 365 days.
                         */
                        if (!isPreviouslyDismissed) {
                            Cookies.set('SlideOutTabDismissed', 'true', 365);
                        }
                    }

                });

                // handle case where we resize the screen
                $window.resize(updateTabPosition);

            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function () {
                // JavaScript to be fired on the home page
                $('.banner-gallery').flexslider({
                    animation: 'fade',
                    directionNav: false,
                    controlNav: true,
                    controlsContainer: ".controls",
                    slideshowSpeed: 6000,
                    animationSpeed: 900,
                    initDelay: 500,
                });
                $('.top-products').flexslider({
                    animation: "slide",
                    animationLoop: false,
                    directionNav: true,
                    controlNav: false,
                    itemWidth: 160,
                    itemMargin: 0,
                    slideshow: false,
                    maxItems: 4,
                    minItems: 2
                });
                $('#solutionTabs .nav a').click(function (e) {
                    e.preventDefault();
                    $(this).tab('show');
                });
                $('#tabSelect').on('change', function (e) {
                    $('#tabNav li a').eq($(this).val()).tab('show');
                });
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
