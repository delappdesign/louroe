<?php
/**
 * The template for displaying the search results
 *
 * @package WordPress
 * @subpackage Louroe
 */

global $post;

/********************************************************************************
 *
 * retrieve product search results, and organize into a sorted array by category
 */
$product_query_args = array(
		'post_type'       => 'product',
		's'               => get_search_query(),
		'posts_per_page'  => -1,
);
$product_query = new WP_Query( $product_query_args );

if ( function_exists('relevanssi_do_query') ){
	relevanssi_do_query( $product_query );
}

/*echo "<pre>";
var_dump($product_query);
echo "</pre>";*/

$product_categories = [];
while ( $product_query->have_posts() ){
	$product_query->the_post();

	$terms = wp_get_post_terms( $post->ID, 'product_cat' );
	foreach( $terms as $term ){
		$product_categories[ $term->slug ][] = $post;
	}

}

$num_products = $product_query->found_posts;

ksort($product_categories);

/**********************************************************************************/


?>

<?php get_template_part( 'templates/page', 'header' ); ?>

<section class="section-panel container content-container groupr" style="background-image: url('<?php echo the_field('page_bg_image'); ?>'); background-repeat: no-repeat">

    <?php // product results added to the wordpress template

			if ( ! empty($product_categories) ) :
    ?>
			<div class="search-group">

				<h2 style="margin-bottom: 10px;">Product Results</h2>
				<p>The following <?= $num_products ?> products matched the search <span class="search-term"><?php echo get_search_query(); ?></span></p>

				<?php

				foreach( $product_categories as $category_slug => $product_category ):

					$category = get_term_by('slug', $category_slug, 'product_cat');
					echo "<h3 class='search-row category'><a href='/product-category/{$category_slug}'>{$category->name}</a></h3>" .
					     "<ul class='product-group'>";

					foreach( $product_category as $product ):

				?>

						<li class="search-item">
							<a href='/product/<?= $product->post_name ?>'>
								<div class="search-image"><?= get_the_post_thumbnail($product->ID, 'shop_thumbnail') ?></div>
							<div class="search-description">
								<strong><?= $product->post_title ?></strong>
								<?php $product_number = function_exists('get_field') ? get_field('product_number', $product->ID) : ''; ?>
								<?php echo $product_number ? "<br>{$product_number}" : ''; ?>
							</div>
							</a>
						</li>

			<?php
					endforeach;

					echo '</ul>';

				endforeach;

				echo '</div>';

			endif;
      ?>


      <?php

      wp_reset_postdata();


      /********************************************************************************
       *
       * retrieve page search results without products
       */

      $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
      $page_query_args = array(
	      'post_type'       => ['post', 'page','project','tribe_events'],
	      's'               => get_search_query(),
	      'posts_per_page'  => 25,
	      'paged'           => $paged,
      );
      $page_query = new WP_Query( $page_query_args );

      if ( function_exists('relevanssi_do_query') ){
	      relevanssi_do_query( $page_query );
      }

      /**********************************************************************************/

        /*echo "<pre>";
				var_dump($page_query);
				echo "</pre>";*/


      if ($page_query->have_posts()) :

        $num_pages = $page_query->found_posts; ?>

	      <h2 style="margin-bottom: 10px;">Page Results</h2>
        <p>The following <?php echo $num_pages; ?> items matched the search <span class="search-term"><?php echo get_search_query(); ?></span></p>

      <?php while ($page_query->have_posts()) : $page_query->the_post(); ?>

        <article class="article-excerpt group">
          <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

	        <?php $excerpt = get_the_excerpt() ?>
	        <?php if ( $excerpt ): ?>
	          <div class="post-content">
	            <?= $excerpt; ?>
	          </div>
		      <?php endif; ?>

        </article>

      <?php
        endwhile;

      endif; ?>

    <nav class="nav-search-pagination group">
      <?php
        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $page_query->max_num_pages
        ) );
      ?>
    </nav>

		<?php wp_reset_postdata(); ?>


		<?php if ( 0 === ($num_products + $num_pages ) ): ?>

			<div class="no-results">
				<h2><?= function_exists('get_field') ? get_field('no_results_message', 'options') : 'Sorry, we didn\'t find any pages that matched your search'; ?></h2>
			</div>

		<?php endif; ?>

</section>