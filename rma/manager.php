<? 

require_once '../include/data_inc.php'; 
require_once '../include/mail_inc.php';

$debug = (isset($_GET['debug'])) ? true : false;
$ipdebug = false; 
//if ( $_SERVER['REMOTE_ADDR'] == '108.23.17.241' ) {  $ipdebug = true; }

date_default_timezone_set('America/Los_Angeles');
$timestamp = date('U');

$pageVariables = 'action,searchCat,searchText,id,wrmaid,rmaid,rmakey,company,address1,address2,city,state,zip,contactName,phone,fax,email,model1,qty1,sn1,model2,qty2,sn2,model3,qty3,sn3,problem,repairReturn,return,rmastatus,ccnumber,ccmonth,ccyear,detail_rows,emailCustomer';
$arrayVariables = explode(",",str_replace(' ','',$pageVariables));
foreach ($arrayVariables as $value) { $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }

$webAuthors = array ( 'OrderDesk'=>'orderdesk@louroe.com', 'Fabi'=>'rma@louroe.com', 'Sara'=>'sara@louroe.com', 'Stephen'=>'stephen@louroe.com', 'Pilar'=>'pilar@louroe.com', 'Salvador'=>'salvador@louroe.com', 'Tony'=>'Techver2@louroe.com', 'Rob'=>'rob@robcsoftware.com' );

//echo "<pre>" . print_r($webAuthors, true) . "</pre>";

$statusGroups['Request'] = array('Entered'=>'', 'Approved'=>'Stephen', 'Pending'=>'' );
$statusGroups['Received'] = array('Credit'=>'Sara', 'Repair'=>'Sara', 'Pending Credit/ Repair'=>'Sara', 'Pending Information'=>'Sara');
$statusGroups['Process'] = array('In Repair'=>'Sara,OrderDesk', 'On Hold'=>'Sara,OrderDesk');
$statusGroups['Payment'] = array('Payment Pending'=>'OrderDesk');
$statusGroups['Complete'] = array('Closed'=>'Sara', 'Credit Issue'=>'Sara', 'Warranty'=>'Sara', 'Paid'=>'Sara');


function emailRecipients($wrmaid) {
		
	global $mysqli;
	global $statusGroups;
	global $webAuthors;
	global $emailCustomer;
	
	//$emailCustomer = 'Y';
	
	// for now send general email 
	$sql = " select * from rma where wrmaid = '$wrmaid' ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in rma select'; exit; }
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {
		foreach ($row as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
	}
	
	// find the recipients
	$temp = explode('-',$RMAStatus);
	$RMAStatusGroup = $temp[0];
	$RMAStatusSub = $temp[1];
		
	$recipients_list = '';	
	$recipients = $statusGroups[$RMAStatusGroup][$RMAStatusSub];
	$recipients = explode(',',$recipients);
	foreach ($recipients as $recipient) {
		$recipient_email = $webAuthors[$recipient];
		$recipients_list .= "{$recipient} {$recipient_email}, ";
	}
	$recipients_list = substr($recipients_list, 0, -2);
	
	
	$RMARequestDate = strtotime( $RMARequestDate );
	$RMARequestDate = date( 'l, Y-m-d h:i A', $RMARequestDate );
	
	$l = "<br>";
	$info_text = '';
	$info_text = "TEMP RECIPIENTS: {$recipients_list}";
	$info_text .= "{$l}{$l}WRMAID: {$WRMAID}, RMAID: {$RMAID} {$l}";
	$info_text .= "{$l}RMA Status: {$RMAStatus}";
	$info_text .= "{$l}{$ContactName}{$l}{$Company}{$l}{$Address1}{$l}{$Address2}";
	$info_text .= "{$l}P: {$Phone}, F: {$Fax}, E: {$Email}";
	$info_text .= "{$l}RMA Request Data: {$RMARequestDate}";
	
	for ($i=1; $i<=5; $i++) {
		$model = ${"Model".$i}; $qty = ${"Qty".$i}; $sn = ${"SerialNo".$i};
		if ($model != '' || $sn != '') {
			$info_text .= "{$l}Model: {$model}, Qty: {$qty}, SN: {$sn}"; 
		}
	}
	
	$info_text .= "{$l}Problem: {$Problem}";
	$info_text .= "{$l}Repair and Return: {$RepairAndReturn}, Return for Credit: {$ReturnForCredit} ";
	$info_text .= "{$l}{$l}Detail History:";
	
	$sql = " select * from rma_details where wrmaid = '$wrmaid' order by WRMA_DetailID desc ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in rma select'; exit; }
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {
		foreach ($row as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };

		$CreateDate = strtotime( $CreateDate );
		$CreateDate = date( 'l, Y-m-d h:i A', $CreateDate );
		
		$info_text .= "{$l}{$l}Date: {$CreateDate}, Author: {$Author}";
		$info_text .= "{$l}Web Notes: {$WebNotes}";
		$info_text .= "{$l}Internal Notes: {$InternalNotes}";
	}	

	// send email

	$mail = new mailWrapper();
	$email = 'rma@louroe.com';
	
	$cc = 'orderdesk@louroe.com; pilar@louroe.com; salvador@louroe.com';
	
	$message  = "<a href='https://www.louroe.com/rma/manager.php?wrmaid={$WRMAID}'>RMA Information</a><br><br>{$info_text}";
	$mail->subject = "RMA Update (W-{$WRMAID}, {$RMAID}): {$Company}, {$ContactName} ~ {$RMAStatus}";

	$to = "$email";
	$mail->body = $message;
	$mail->to = $email;
	//$mail->bcc = $bcc;
	$mail->cc = $cc;
	$mail->from = "info@louroe.com";
	$mail->fromName = 'Louroe RMA Info';
	
	if ($emailCustomer != '') {
		$mail->send();
		$description = "Email Cust: {$RMAStatus}";
		$sql = " insert into rma_history ( WRMAID, Description ) values ( '{$WRMAID}', '{$description}' ) ";
		$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in rma insert history'; exit; }
	}
	
}



//$statusGroups = json_decode($statusGroups, true);

//echo "<pre>" . print_r($statusGroups, true) . "</pre>";
//exit;




ksort($webAuthors);

if ($action == 'listing') {
	
	$sql = " select * from rma where 1=1 ";
	if ($searchCat == 'Active') { $sql .= " and ( left(RMAStatus,8) != 'Complete' ) "; }
	if ($searchCat == 'Completed') { $sql .= " and ( left(RMAStatus,8) = 'Complete' ) "; }
	
	if ($searchText != '') { $sql .= " and ( Company like '%{$searchText}%' or ContactName like '%{$searchText}%' or Email like '%{$searchText}%' or WRMAID like '%{$searchText}%' ) "; }
	$sql .= " order by Company ";
	
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing'; exit; }
	
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {	
		$result[] = $row;
	}
	$result = json_encode($result);
	echo $result;
	exit;
}

if ($action == 'detail') {
	
	$sql = " select * from rma_details where wrmaid = '$id' order by WRMA_DetailID ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing'; exit; }
	
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {	
		
		$datetime = strtotime($row['CreateDate']);
		
		$dateformatted = date("m-d-Y h:i A", $datetime);
		$row['CreateDateTime'] = $dateformatted;		
		
		$dateformatted = date("m-d-Y", $datetime);
		$row['CreateDate'] = $dateformatted;

		$details[] = $row;

	}	
	
	$sql = " select * from rma_history where wrmaid = '$id' order by RMAHistoryID desc ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing history'; exit; }
	
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {	
		
		$datetime = strtotime($row['CreateDate']);
		$dateformatted = date(" Y-m-d h:i A", $datetime);
		$row['CreateDate'] = $dateformatted;
		$history[] = $row;
	}
	
	$sql = " select * from rma where wrmaid = '$id' ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing'; exit; }
	
	if ( $row = mysqli_fetch_assoc($mysqli_result) ) {	
		$datetime = strtotime($row['RMARequestDate']);
		$dateformatted = date("m-d-Y", $datetime);
		$row['RMARequestDate'] = $dateformatted;
		
		$row['rma-details'] = $details;
		$row['rma-history'] = $history;
		$result[] = $row;
	}
	

	$result = json_encode($result);
	echo $result;
	exit;
}

if ($action == 'update')
{
	$error = false;
	$debug = false;
	if ($debug) { echo "<pre>" . print_r($_GET, true) . "</pre>"; }
	//exit;
	
	parse_str($_SERVER['QUERY_STRING'], $get_array);
	$problem = $get_array['problem'];

	//$sql = "insert into rma (Company) values ('$Company') ";
	//$mysqli_result_product = mysqli_query($mysqli, $sql); if (!$mysqli_result_product) { echo 'error in rma insert'; exit; }
	//$WRMAID = mysqli_insert_id($mysqli);

	//$RMAKey = randString(5) . '-' . randString(5);

	$repairReturn = ($repairReturn != '') ? 'Y' : '';
	$return = ($return != '') ? 'Y' : '';
	
	$ccexpiration = "{$ccmonth}-{$ccyear}";


	
	// escape variables for mysqli
	$escape_fields = "name, company, address1, address2, problem";
	$escape_array = explode(",",$escape_fields);
	foreach ($escape_array as $value) { $value = trim($value); $$value = mysqli_real_escape_string($mysqli, $$value); }

	$sql = "update rma set RMAID = '$rmaid', Company = '$company', Address1 = '$address1', Address2 = '$address2',
	City = '$city', State = '$state', Zip = '$zip',
	ContactName = '$contactName', Phone = '$phone', Fax = '$fax', Email = '$email', Model1 = '$model1', Qty1 = '$qty1', SerialNo1 = '$sn1',
	Model2 = '$model2', Qty2 = '$qty2', SerialNo2 = '$sn2', Model3 = '$model3', Qty3 = '$qty3', SerialNo3 = '$sn3', 
	Model4 = '$model4', Qty4 = '$qty4', SerialNo4 = '$sn4', Model5 = '$model5', Qty5 = '$qty5', SerialNo5 = '$sn5',
	Problem = '$problem', RepairAndReturn = '$repairReturn', ReturnForCredit = '$return', CCNumber = '$ccnumber', CCExpiration = '$ccexpiration',
	RMAStatus = '$rmastatus'
	where WRMAID = '$wrmaid' ";

	if ($ipdebug) { echo $sql; }
	$mysqli_result_product = mysqli_query($mysqli, $sql); if (!$mysqli_result_product) { echo "error in rma update:<br><br>$sql<br><br>" . mysqli_error($mysqli); exit; }
	
	// add the detail rows and set up as a transaction
	try {
    	// First of all, let's begin a transaction
    	mysqli_autocommit($mysqli, FALSE);
    	//mysqli_begin_transaction($mysqli);
    	
    	/*
		$sql = "delete from rma_details where WRMAID = '$wrmaid'";
		$rs = mysqli_query($mysqli, $sql);
		if (!$rs) { throw new Exception("Could not execute query: $sql\n"); }
		*/

		

		for ($i=1; $i<=$detail_rows; $i++) {
		
			$wrma_detailid = $_GET["wrma_detailid_{$i}"];
			$web_note = $get_array["webnote_{$i}"];
			$note = $get_array["note_{$i}"];
			$author = $_GET["author_{$i}"];
					
			$web_note = str_replace("'","\'",$web_note);
			$note = str_replace("'","\'",$note);
					
			// update the record if it exists
			if ($wrma_detailid != '') {
			
				if ($web_note == '' && $note == '') {
					$sql = "delete from rma_details where WRMA_DetailID = '$wrma_detailid' ";
					if ($debug) { echo $sql; }
					$rs = mysqli_query($mysqli, $sql); if (!$rs) { throw new Exception("Could not execute query: $sql\n"); }
				} else {
					$sql = "update rma_details set WebNotes = '$web_note', InternalNotes = '$note', Author = '$author' where WRMA_DetailID = '$wrma_detailid' ";
					if ($debug) { echo $sql; }
					$rs = mysqli_query($mysqli, $sql); if (!$rs) { throw new Exception("Could not execute query: $sql\n"); }
				}
			
			} else {
				
				// insert new record
				if ($web_note != '' || $note != '') {
					$sql = "insert into rma_details ( WRMAID, WebNotes, InternalNotes, Author ) 
					values ( '$wrmaid', '$web_note', '$note', '$author' ) ";
					if ($debug) { echo $sql; }
					$rs = mysqli_query($mysqli, $sql); if (!$rs) { throw new Exception("Could not execute query: $sql\n"); }
				}
				
			}

		}

    	mysqli_commit($mysqli);
    	
		} catch (Exception $e) {
			echo 'exception';
    		mysqli_rollback($mysqli);
    		print_r($e);
    		
    		$result['error'] = 'Y';
    		$result = json_encode($result);
    		$error = true;
		}
	
	mysqli_autocommit($mysqli, TRUE);
	
	//$error = true;
	if ($wrmaid==32) { $error = true; }
	if (!$error) { emailRecipients($wrmaid); }
	
	$sql = " select * from rma_history where wrmaid = '$wrmaid' order by RMAHistoryID desc ";
	$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { echo 'error in listing history'; exit; }
	
	$history = array();
	while ( $row = mysqli_fetch_assoc($mysqli_result) ) {	
		$datetime = strtotime($row['CreateDate']);
		$dateformatted = date(" Y-m-d h:i A", $datetime);
		$row['CreateDate'] = $dateformatted;
		$history[] = $row;
	}
	$result[0]['rma-history'] = $history;
	$result = json_encode($result);
	echo $result;
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Louroe - RMA Admin</title>
	<meta name="description" content="" />
	<script src="/js/jquery-1.10.2.min.js"></script>
	<script src="manager-code.js?<?=$timestamp?>"></script>
	<link rel="stylesheet" type="text/css" href="manager-style.css?<?=$timestamp?>">
</head>

<script>
	var selectedWRMAID = '<?=$wrmaid?>';
	
	function validate(info) {
	<? if ($debug) { ?>
		alert(info);
	<? } ?>
	}
</script>


<body>
<div class="container">

	<div id="main_left">
		<div id="main_left_search">
			Search: <input type=text name=search id=search>
			<select name=search_cat id=search_cat>

			<? 
				//foreach ($statusGroups as $key => $value ) { echo "<option value='{$key}'>{$key}</option>"; }
			?>
				<option value='Active'>Active</option>
				<option value='Completed'>Completed</option>
			</select>
			<a href="manager-export.php"><img src="img/export.png" width=20 height=20 align=top></a>
		</div>
		<div id="main_left_listing"></div>
	</div>
	
	<div id="main_right">
	
	
		<div class="rma-form-containter">
	
				<div class="rma-form-box">
					<form id="rma-form" action="" method=post>
						
					<div class="rma-section" style="margin-top:10px; margin-left:10px; important!">
						<div class="column-0-1">
							<div class="row label" >RMA#</div>
						</div>
						<div class="column-0-2">
							<div class="row label"><input type=text name=rmaid id=rmaid class="" value="" ></div>
						</div>
						<div class="column-0-1">
							<div class="row label">WRMA#</div>
						</div>
						<div class="column-0-2">
							<div class="row label"><input type=text id=wrmaid name=wrmaid value="<?=$wrmaid?>" ></div>
						</div>
						<div class="column-0-1">
							<div class="row label">Key</div>
						</div>
						<div class="column-0-2">
							<div class="row label"><input type=text name=rmakey id=rmakey value="" ></div>
						</div>
						<div class="column-0-1">
							<div class="row label">Date</div>
						</div>
						<div class="column-0-2">
							<div class="row label"><input type=text name=rmarequestdate id=rmarequestdate value="" ></div>
						</div>
						<div class="column-0-3">
							<div class="row label"><input id=rma-submit name=submit type=button value="Save" style="width:80px !important">&nbsp;
							<input id=rma-submit-email name=submit type=button value="Save & Email" style="width:80px !important"></div>
						</div>
					</div>
						
					<div class="rma-section" style="margin-top:10px; margin-left:10px; important!">	
						<div class="column-1-1">
							<div class="row label-required" title="company">Company</div>
							<div class="row label">Address</div>
							<div class="row label">&nbsp;</div>
							<div class="row label-required" title="city">City</div>
							<div class="row label-required" title="state">State</div>
							<div class="row label-required" title="zip">Zip</div>
							<div class="row label-required" title="contactName">Contact Name</div>
							<div class="row label-required" title="phone">Phone</div>
							<div class="row label">Fax</div>
							<div class="row label-required" title="email">Email</div>
						</div>
						<div class="column-1-2">
							<div class="row "><input type=text name=company id=company class="required" value="" ></div>
							<div class="row"><input type=text name=address1 id=address1 value="" ></div>
							<div class="row"><input type=text name=address2 id=address2 value="" ></div>
							<div class="row"><input type=text name=city id=city value="" ></div>
							<div class="row"><input type=text name=state id=state value="" ></div>
							<div class="row"><input type=text name=zip id=zip value="" ></div>
							<div class="row"><input type=text name=contactName id=contactName class="required" value="" ></div>
							<div class="row"><input type=text name=phone id=phone class="required" value="" ></div>
							<div class="row"><input type=text name=fax id=fax value="" ></div>
							<div class="row"><input type=text name=email id=email class="required" value="" ><img id=email-button src="/img/icon-mail-button.png"></div>
						</div>
						<div class="column-1-3">
							
							<div class="row label-required" title="request">Request</div>
							<div class="row label"><input type=checkbox name=repairReturn id=repairReturn>&nbsp;Repair</div>
							<div class="row label"><input type=checkbox name=return id=return>&nbsp;Return for Credit</div>
							<!--
							<div class="row label">CC Number</div>
							<div class="row"><input type=text name=ccnumber id=ccnumber class="" value="" style="width:200px !important" ></div>
							<div class="row">
								<select id=ccmonth name=ccmonth>
									<option value=0>Month</option>
								<? for ($i=1; $i <= 12; $i++ ) { 
									$monthname = date("M",mktime(0,0,0,$i,1,2000)); 
									echo "<option value='{$i}'>{$monthname}</option>";
								} ?> 
								</select>
								&nbsp;
								<select id=ccyear name=ccyear>
									<option value=0>Year</option>
								<? for ($i=2013; $i <= 2021; $i++ ) { 
									echo "<option value='{$i}'>{$i}</option>";
								} ?> 
								</select>							
							</div>
							-->
						</div>
						<div class="column-1-4">
							<div class="row">&nbsp;</div>
							<div class="row label">RMA Status</div>
							<div class="row">
								<select id=rmastatus name=rmastatus>
								<? foreach ($statusGroups as $key => $value ) {
									$aKey = $key;
									echo "<optgroup label='$aKey'>";
									foreach ($statusGroups[$aKey] as $key => $value) {
									echo "<option value='{$aKey}-{$key}'>{$aKey}-{$key}</option>";
									}
									echo "</optgroup>";
								}
								?>
								</select>
						</div>
						
						
					</div>
			
					<div id="rma-products" class="rma-section" style="">
						<div class="column-2-1">
							<div id="modellabel_1" class="row label-required" title="model1" >Model</div>
							<div id="modellabel_2"  class="row label">Model</div>
							<div id="modellabel_3"  class="row label">Model</div>
							<div id="modellabel_4"  class="row label">Model</div>
							<div id="modellabel_5"  class="row label">Model</div>
						</div>
						<div class="column-2-2">
							<div class="row label"><input type=text name=model1 id=model1 class="required" value="" tabindex=200></div>
							<div class="row label"><input type=text name=model2 id=model2 class="model" value="" tabindex=205 ></div>
							<div class="row label"><input type=text name=model3 id=model3 class="model" value=""  tabindex=210 ></div>
							<div class="row label"><input type=text name=model3 id=model4 class="model" value="" tabindex=215 ></div>
							<div class="row label"><input type=text name=model3 id=model5 class="model" value="" tabindex=220 ></div>
						</div>
						<div class="column-2-1">
							<div id="qtylabel_1" class="row label-required">Qty</div>
							<div id="qtylabel_2" class="row label">Qty</div>
							<div id="qtylabel_3" class="row label">Qty</div>
							<div id="qtylabel_4" class="row label">Qty</div>
							<div id="qtylabel_5" class="row label">Qty</div>
						</div>
						<div class="column-2-2">
							<div class="row label-required"><input type=text name=qty1 id=qty1 class="required" value=""  tabindex=201></div>
							<div class="row label"><input type=text name=qty2 id=qty2 value="" tabindex=206 ></div>
							<div class="row label"><input type=text name=qty3 id=qty3 value="" tabindex=211 ></div>
							<div class="row label"><input type=text name=qty3 id=qty4 value="" tabindex=216 ></div>
							<div class="row label"><input type=text name=qty3 id=qty5 value="" tabindex=221 ></div>
						</div>
						<div class="column-2-1">
							<div id="snlabel_1" class="row label">Serial #</div>
							<div id="snlabel_2" class="row label">Serial #</div>
							<div id="snlabel_3" class="row label">Serial #</div>
							<div id="snlabel_4" class="row label">Serial #</div>
							<div id="snlabel_5" class="row label">Serial #</div>
						</div>
						<div class="column-2-2">
							<div class="row label"><input type=text name=sn1 id=sn1 value="" tabindex=202></div>
							<div class="row label"><input type=text name=sn2 id=sn2 value="" tabindex=207></div>
							<div class="row label"><input type=text name=sn3 id=sn3 value="" tabindex=212 ></div>
							<div class="row label"><input type=text name=sn3 id=sn4 value="" tabindex=217 ></div>
							<div class="row label"><input type=text name=sn3 id=sn5 value="" tabindex=222></div>
						</div>
						<div class="column-2-3">
							<div class="row label">&nbsp;<div id="warrantylabel_1" style="display:inline-block;" ><input type=checkbox name=warranty1 id=warranty1 class="warranty" tabindex=203>Warranty</div></div>
							<div class="row label">&nbsp;<div id="warrantylabel_2" style="display:inline-block;" ><input type=checkbox name=warranty2 id=warranty2 class="warranty" tabindex=208>Warranty</div></div>
							<div class="row label">&nbsp;<div id="warrantylabel_3" style="display:inline-block;" ><input type=checkbox name=warranty3 id=warranty3 class="warranty" tabindex=213>Warranty</div></div>
							<div class="row label">&nbsp;<div id="warrantylabel_4" style="display:inline-block;" ><input type=checkbox name=warranty4 id=warranty4 class="warranty" tabindex=218>Warranty</div></div>
							<div class="row label">&nbsp;<div id="warrantylabel_5" style="display:inline-block;" ><input type=checkbox name=warranty5 id=warranty5 class="warranty" tabindex=223>Warranty</div></div>
						</div>
					</div>
				
					<div class="rma-section" style="margin-left:10px; important! ">
						<div class="column-2-1">
							<div class="row label-required" title="problem">Problem</div>
						</div>
						<div class="column-2-2-2">
							<div class="row label"><textarea name=problem id=problem class="required" ></textarea></div>
						</div>
						<div class="column-2-4">
							<div class="row label">History</div>
						</div>
						<div class="column-2-5">
							<div class="row label"><div id="email-history"></div></div>
						</div>
					</div>

					<div class="rma-section" style="margin-left:10px; important!">
						<div style="display:block">
							<div class="row label" title="">Details</div>
						</div>
						<div id=rma-details-header name=rma-details-header>
							<div class=rma-header-row>
								<div style="width:75px; display:inline; float:left; padding-left:0px; " >Date</div>
								<div style="width:300px; display:inline; float:left; padding-left:5px; " >Web Notes</div>
								<div style="width:300px; display:inline; float:left; padding-left:0px; " >Internal Notes</div>
								<div style="width:50px; display:inline; float:left; padding-left:10px; " ><input type=button id=rma-detail-add value='Add'></div>
							</div>
						</div>
						<input type=hidden name=detail_rows id=detail_rows value=0>
						<div id=rma-details name=rma-details>
						
							<div class=rma-template-row>
								<input type=hidden id="wrma_detailid_" name="wrma_detailid_" class="detail-id">
								<div id="notedate_" name="notedate_" class="detail-date">Today</div>
								<textarea id="webnote_" name="webnote_" class=detail-webnote></textarea>&nbsp;
								<textarea id="note_" name="note_" class=detail-note></textarea>&nbsp;
								<select id="author_" name="author_" class=detail-author>
									<option value=''>Author</option>
									<? foreach ($webAuthors as $key=>$value) { echo "<option value='$key'>$key</option>"; }
									?>
								</select>
							</div>
							
							<div class=rma-footer-row>
							</div>
						
						</div>
					</div>
				

				
					</form>
				</div>
		</div> <!-- end rma-form-container -->
	


	</div> <!-- end main_right -->
	
	
</div>
<div id="system-status">Updating</div>
	
</body>
</html>
