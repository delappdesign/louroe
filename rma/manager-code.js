
$(document).ready(function() {

	$(".rma-form-containter :input").attr("disabled", true);
	$("#email-button").css({'cursor':'default'});
	
	var emailCustomer='';
	
	$("#rma-submit").click(function() {
		emailCustomer='';
		rmaSubmit();
	});
	
	$("#rma-submit-email").click(function() {
		emailCustomer='Y';
		rmaSubmit();
	});
	
	
	function rmaSubmit() {
		
		// clear all fields
		$(".label-required").removeClass("error");
		$("#form-error").html('Required Fields');
		$("#form-error").removeClass("error");
		
		
		// determine required fields
		var error = false;
		/*
		$('.required').each(function() {
			field = this.id;
			if ($("#"+field).val()=='') {
				error = true;
				$('div[title="'+field+'"]').addClass("error");
			}
		});
		
		if (!$('#repairReturn').prop('checked') && !$('#return').prop('checked')) {
			error = true;
			$('div[title="request"]').addClass("error");
		}
		*/
		//error = false
		if (error) {
			$("#form-error").html('Please review the required fields.');
			$("#form-error").addClass("error");
		} else {
		
			// submit the form data
			
			data = $("#rma-form").serialize();

			wrmaid = $("#wrmaid").val();
			url = 'manager.php?action=update&emailCustomer='+emailCustomer+'&wrmaid=' + wrmaid + "&" + data;
			validate(url);
			//window.open(url,'_blank');
			showStatus('Saving Changes');
			$.ajax( { type: "GET", url: url, data: '', async: true,
				success: function(html) { 
					//populateListing($('#search').val());
					//console.log('html result: ', html);
					populateHistory(html);
					hideStatus();
					
				}
			}); 
			
		}
	
	}

	
	function populateListing(searchText) {
		
		url = 'manager.php?action=listing&searchCat=' + $('#search_cat').val() + '&searchText='+searchText;
		validate(url);
		$.ajax( { type: "GET", url: url, data: '', async: true,
			success: function(html) { 
				populateListingSuccess(html);
			}
		}); 
		
	}
	
	var count = 0;
	function populateListingSuccess(data) {
		
		var wrmaid = selectedWRMAID;
		selectedWRMAID = 0;
		

		
		//alert($("#wrmaid").val() + '---' + wrmaid );
		
		count++;
		//console.log('data:', data);
		var json = jQuery.parseJSON(data);
		if (json != null && json != '') {

			var text = '';
			json_rows = json.length;
			
			$("#main_left_listing").html('');
			var list = $("#main_left_listing").append('<ul id=list ></ul>').find('ul');
			
			for ( i=0; i<json_rows; i++ ) {
				id = json[i]['WRMAID'];
				selected = '';
				if (id == wrmaid || id == $("#wrmaid").val() ) { selected = 'selected-item'; }
				text = json[i]['Company'];
				
				new_item = '';
				if ( json[i]['RMAStatus'] == 'Request-Entered' ) {
					new_item = 'new-item';
					text = text + ' (entered) ';
				}
				
				
				if ( json[i]['ContactName'] == undefined ) { json[i]['ContactName'] = ''; }
				if ( json[i]['Email'] == undefined ) { json[i]['Email'] = ''; }
				
				if ( json[i]['Email'] != '' ) { text = text + '<p class="listing-email">' +  json[i]['Email']; }
				if ( json[i]['ContactName'] != '' ) { text = text + '<p class="listing-contactname">' + json[i]['ContactName']; }
				
				

				
				list.append('<li class="listing-item ' + new_item + ' ' + selected + '" wrma-id="' + id +'" >' + text + '</li>');
			}

		} else {
			alert('No Search Results.');
		}
		
		// populate detail
		if (wrmaid != 0) { populateDetail(wrmaid); } 

	}
	
	function populateHistory(data) {

		$("#email-history").html('');
		var json = jQuery.parseJSON(data);
		if (json != null && json != '') {

			if ( json[0]['rma-history'] != null ) {
				var rma_history_items = json[0]['rma-history'].length;
				html = '';
				for (i = 1; i <= rma_history_items; i++) {
					html += '<b>' + json[0]['rma-history'][i-1]['CreateDate'] + '</b> ' + json[0]['rma-history'][i-1]['Description'] + '<br>';
				}
				$("#email-history").html(html);
			}

			}
	}
	
	$('#search').keyup(function(event) {
		if (event.which == 13) {
			searchText = $('#search').val();
			populateListing(searchText);
		}
	});
	
	/*
	$("body").keypress(function(event) {
	alert(event.which);
    if (event.which == 13) {

    }
    });
    */
	
	$('#search_cat').change(function(event) {
		$("#search").val('');
		populateListing('');
	});
	
	$("#email-button").click(function(event) {
		if ($("#wrmaid").val() != '' && $("#wrmaid").val() != 0 ) {
			subject = "Your RMA Request";
			if ( $("#contactName").val() != '') { 
				body = "Hello " + $("#contactName").val() + "." + "%0D%0A%0D%0A%0D%0A%0A" + "You can review your RMA status here: ";
				body = body + "https://www.louroe.com/rma-portal.php?email=";
				body = body + $("#email").val() + "%26rmakey=" + $("#rmakey").val();
			}
			window.location.href= "mailto:" + $("#email").val() + "?subject=" + subject + "&body=" + body;
		}
		
    });
	
	populateListing('');
	
	
	$("#main_left_listing").on('click', '.listing-item', function() {
		if (inUpdate) { return; }
		$('.listing-item').removeClass('selected-item');
		$(this).addClass('selected-item');
		id = $(this).attr("wrma-id");
		populateDetail(id);
	});
	
	function populateDetail(id) {

		url = 'manager.php?action=detail&id='+id;
		validate(url);
		showStatus('Retrieving Data');
		$.ajax( { type: "GET", url: url, data: '', async: true,
			success: function(html) { 
				populateDetailSuccess(html);
				hideStatus();
			}
		}); 	
	}
	
	function populateDetailSuccess(data) {
	
		// clear all fields
		$(".label-required").removeClass("error");
		$("#form-error").html('Required Fields');
		$("#form-error").removeClass("error");

		$("#repairReturn").prop('checked', false);
		$("#return").prop('checked', false);
		
		$("#rmastatus").val('New');
		$("#ccmonth").val(0);
		$("#ccyear").val(0);
		
		$("#email-history").html('');
		
		// remove all detail rows other than template
		$(".rma-detail-row").remove();
		$("#detail_rows").val(0);
		$("#wrma_detailid").val('');
		$("#webnote_1").val('');
		$("#note_1").val('');
		$("#author_1").val('');
		
		var json = jQuery.parseJSON(data);
		
		if (json != null && json != '') {
			// populate fields
			$("#rmaid").val(json[0]['RMAID']);
			$("#wrmaid").val(json[0]['WRMAID'])
			$("#company").val(json[0]['Company']);
			$("#address1").val(json[0]['Address1']);
			$("#address2").val(json[0]['Address2']);
			$("#city").val(json[0]['City']);
			$("#state").val(json[0]['State']);
			$("#zip").val(json[0]['Zip']);
			$("#contactName").val(json[0]['ContactName']);
			$("#phone").val(json[0]['Phone']);
			$("#Fax").val(json[0]['Fax']);
			$("#email").val(json[0]['Email']);
			$("#problem").val(json[0]['Problem']);
			for (i=1; i<=3; i++) {
				$("#model"+i).val(json[0]['Model'+i]);
				$("#qty"+i).val(json[0]['Qty'+i]);
				$("#sn"+i).val(json[0]['SerialNo'+i]);
			}
			
			
			if ( json[0]['RepairAndReturn'] == 'Y' ) { $("#repairReturn").prop('checked', true); } 
			if ( json[0]['ReturnForCredit'] == 'Y' ) { $("#return").prop('checked', true); } 
			
			$("#ccnumber").val(json[0]['CCNumber']);
			$("#rmakey").val(json[0]['RMAKey']);
			$("#rmarequestdate").val(json[0]['RMARequestDate']);
			
			// set the selects
			if ( json[0]['CCExpiration'] == null ) { json[0]['CCExpiration'] = ''; }
			
			split = json[0]['CCExpiration'].split('-');
			ccmonth = split[0];
			ccyear = split[1];
			
			$('#rmastatus option[value="'+ json[0]['RMAStatus'] + '"]').prop('selected', true);
			$('#ccmonth option[value="'+ ccmonth + '"]').prop('selected', true);
			$('#ccyear option[value="'+ ccyear + '"]').prop('selected', true);
			
			// populate the detail items
			if ( json[0]['rma-details'] != null ) {
				var rma_detail_items = json[0]['rma-details'].length;
			
				for (i = 1; i <= rma_detail_items; i++) {
				
					createDetailRow();

					$("#notedate_"+ i).html(json[0]['rma-details'][i-1]['CreateDate']);
					$("#detaildate_"+ i).html(json[0]['rma-details'][i-1]['CreateDateTime']);
					$("#wrma_detailid_"+ i).val(json[0]['rma-details'][i-1]['WRMA_DetailID']);
					$("#webnote_"+ i).val(json[0]['rma-details'][i-1]['WebNotes']);
					$("#note_"+ i).val(json[0]['rma-details'][i-1]['InternalNotes']);
					$("#author_"+ i).val(json[0]['rma-details'][i-1]['Author']);
			
				}

			}

			// populate the history items
			populateHistory(data);
			
			
			
			selectedWRMAID = $("#wrmaid").val();
		} // end json if
		
		
	}
	
	$("#rma-details-header").on('click', '#rma-detail-add', function() {
		createDetailRow();
	});
	
	
	function createDetailRow() {
	
		var d = new Date();
		ampm = 'AM';
		hours = d.getHours();
		minutes = d.getMinutes();
		if (hours >= 12) { hours = hours - 12; ampm = 'PM'; }
		if (hours == 0) { hours = 12; }
		if (minutes < 10) { minutes = "0" + minutes; }
		
		var today = (d.getMonth()+1) + '/' + (d.getDate()) + '/' + (d.getFullYear()) + '<br>' + (hours) + ':' + (minutes) + ' ' + ampm;

	
		var detail_rows = $("#detail_rows").val();
		detail_rows++;
		var clone = $( ".rma-template-row" ).clone().attr({"class":"rma-detail-row"}).insertAfter( ".rma-template-row" );
		
		clone.find('.detail-id').attr({'id':'wrma_detailid_'+detail_rows,'name':'wrma_detailid_'+detail_rows,'value':''});
		clone.find('.detail-date').attr({'id':'detaildate_'+detail_rows,'name':'detaildate_'+detail_rows});
		clone.find('.detail-date').html(today);
		clone.find('.detail-webnote').attr({'id':'webnote_'+detail_rows,'name':'webnote_'+detail_rows});
		clone.find('.detail-note').attr({'id':'note_'+detail_rows,'name':'note_'+detail_rows});
		clone.find('.detail-author').attr({'id':'author_'+detail_rows,'name':'author_'+detail_rows});
		
		$("#detail_rows").val(detail_rows);
	}
	
	var inUpdate = false;
	function showStatus(text) {
		inUpdate = true;
		$('#system-status').html(text);
		$(".rma-form-containter :input").attr("disabled", true);
		$("#email-button").css({'cursor':'default'})
		$('#system-status').show();

	}

	function hideStatus() {
		inUpdate = false;
		if (selectedWRMAID !='') { $(".rma-form-containter :input").attr("disabled", false); }
		$("#email-button").css({'cursor':'pointer'});
		$("#rmakey").attr("disabled", true);
		$("#wrmaid").attr("disabled", true);
		$("#rmarequestdate").attr("disabled", true);
		
		$('#system-status').hide();

	}

});