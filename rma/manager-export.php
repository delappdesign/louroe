<?


require_once '../include/data_inc.php'; 
require_once '../include/mail_inc.php';

$timestamp = date('U');

$debug = isset($_GET['debug']) ? true : false;

// get page variables
$pageVariables  = "";
$arrayVariables = explode(",",str_replace(' ','',$pageVariables));
foreach ($arrayVariables as $value) { $$value=''; if (isset($_POST[$value])) { $$value = $_POST[$value]; } else { if (isset($_GET[$value])) { $$value = $_GET[$value]; } } }


//$debug = true;

$dt = new DateTime();
$theDate = date_format($dt, 'Y-m-d');

$filename ="rma_export_{$theDate}.xls";
$output_format = 'report';


if (!$debug) { 
	header("Content-type: text/csv");
	header("Cache-Control: no-store, no-cache");
	header("Content-Disposition: attachment; filename=\"$filename\"");
}

// WRMAID,RMAID,RMAKey,Company,Address1,Address2,ContactName,Phone,Fax,Email,Model1,Qty1,SerialNo1,Model2,Qty2,SerialNo2,
// Model3,Qty3,SerialNo3,Model4,Qty4,SerialNo4,Model5,Qty5,SerialNo5,Problem,RepairAndReturn,ReturnForCredit,
// RMARequestDate,RMAStatus,CCNumber,CCExpiration

$sql = "select WRMAID,RMAID,RMAKey as 'RMA Key',Company,Phone,Problem,RepairAndReturn as 'Repair And Return',ReturnForCredit as 'Return for Credit',
RMARequestDate as 'Request Date',RMAStatus as 'Status', '' as Notes from rma order by wrmaid ";
$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { SQLError($sql); }

echo "<table border=1 cellpadding=2 style='border-width:1px;border-spacing: 2px;border-style: 
solid;border-color:gray;border-collapse:collapse;background-color:white;' >";
echo "\n<tr>"; 

$finfo = mysqli_fetch_fields($mysqli_result);
foreach ($finfo as $val) { 
	echo "<td>" . $val->name . "</td>";
}

echo "</tr>";

$num_fields = count($field_names);	


$i = 0;
while ($row = mysqli_fetch_assoc($mysqli_result)) {

	// retrieve notes
	$notes = '';
	$wrmaid = $row['WRMAID'];
	$sql = "select CreateDate,WebNotes,InternalNotes,Author from rma_details where wrmaid = '$wrmaid' order by CreateDate ";
	$mysqli_detail_result = mysqli_query($mysqli, $sql); if (!$mysqli_detail_result) { SQLError($sql); }
	while ($row_detail = mysqli_fetch_assoc($mysqli_detail_result)) {
	
		$row_detail['CreateDate'] = date('Y-m-d g:i A', strtotime($row_detail['CreateDate']));	
		$notes .= "\n" . $row_detail['CreateDate'] . ", ";
		if ($row_detail['WebNotes'] != '') { $notes .= "(Web) " . $row_detail['WebNotes'] . " "; }
		if ($row_detail['InternalNotes'] != '') { $notes .= " (Internal) " . $row_detail['InternalNotes'] .  " "; }
		$notes .= "by " . $row_detail['Author'];
	}

	echo "\n<tr>";
	$row['Notes'] = $notes;
	$row['Request Date'] = date('Y-m-d, g:i A', strtotime($row['Request Date']));
	$link = "<a href='https://www.louroe.com/rma/manager.php?wrmaid=" . $row['WRMAID'] . "'>" . $row['WRMAID'] . "</a>";
	$row['WRMAID'] = $link;
	
	foreach ($row as $key => $value ) { 
		echo "<td>" . $value . "</td>";
	}; 
	echo "</tr>";


		
}
	



?>