<? require_once 'include/data_inc.php'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Louroe</title>
<meta name="keywords" content="audio, survelience" />
<meta name="description" content="Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..." />
<link href="css/primary.css" rel="stylesheet" type="text/css" />
<link href="css/home.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type='text/javascript' src='js/quickmenu.js'></script>
</head>

<body>
<div class="container">
	<? require 'header_map_inc.php'; ?>
  <div class="bodyContent">
    <div class="leftContent">
      <div class="welcomeTitle">Contact.</div>
      <div class="welcomeTxt">
    			<img src="img/home/customer-service-female.jpg" width="281" height="172" align="right" class="spaceLB" />
    
	      <p><strong>Office Location:</strong><br />
          6955 Valjean Ave.<br />
          Van Nuys, CA 91406<br />
        </p>
        <p><strong>Telephone Numbers:</strong><br />
          Tel: (818) 994-6498<br />
        Fax: (818) 994-6458</p>
        <p>Call us for designing a security, monitoring, or intercommunication <br />
          system or for further information on any of our products.<br />
        </p>
        <p><strong>E-mail:<br />
        </strong><a href="mailto:sales@louroe.com">sales@louroe.com</a><br />
        For all questions on sales and marketing, products, or data</p>
        <p><a href="mailto:info@louroe.com">info@louroe.com</a><br />
        For technical support and other inquiries.</p>
        <p><a href="mailto:orderdesk@louroe.com">orderdesk@louroe.com</a><br />
          For placing orders, pricing information and shipping information.</p>
        <p><a href="mailto:webmaster@louroe.com">webmaster@louroe.com</a><br />
          For any questions about this website.</p>
      </div>
   	</div>
				<? require 'sidebar_right_inc.php'; ?>
		</div>
	<? require 'footer_inc.php'; ?>
</div>
</body>
</html>
