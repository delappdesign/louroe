<? 

error_reporting(E_ALL);
ini_set( 'display_errors','1'); 
ini_set('include_path', ini_get('include_path').';../Classes/');

require_once '../include/data_basic_inc.php'; 
require_once '../include/wp_functions_inc.php';

// PHPExcel
require_once '../include/phpexcel_inc/PHPExcel.php'; 
require_once '../include/phpexcel_inc/PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Products');

$base_dir = '/home1/louroeco/public_html/';

// descriptionWeb, descriptionShort

$sql_product = "select p.*, c.CategoryFolder, c.CategoryName, cs.SubCategoryName from products p 
left join categories c on p.CategoryID = c.CategoryID
left join categories_sub cs on p.SubCategoryID = cs.SubCategoryID
where p.active = 'Y' order by ProductSerial";
$mysqli_result_product = mysqli_query($mysqli, $sql_product); if (!$mysqli_result_product) { SQLError($sql_product); }

$row = 1;
$objPHPExcel->getActiveSheet()->SetCellValue("A{$row}", "Product Serial");
$objPHPExcel->getActiveSheet()->SetCellValue("B{$row}", "Product Name");
$objPHPExcel->getActiveSheet()->SetCellValue("C{$row}", "Product Category");
$objPHPExcel->getActiveSheet()->SetCellValue("D{$row}", "Product URL");
$objPHPExcel->getActiveSheet()->SetCellValue("E{$row}", "Product Description");
$objPHPExcel->getActiveSheet()->SetCellValue("F{$row}", "Product Description Short");
$objPHPExcel->getActiveSheet()->SetCellValue("G{$row}", "Product Keywords");

$width = 50;
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth($width);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth($width);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth($width);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth($width);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(100);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth($width);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth($width);


while($row_product = mysqli_fetch_assoc($mysqli_result_product)) {
	foreach ($row_product as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
	$row++;
	
	// check for product image in directory
	$image = "products/images/{$CategoryFolder}/{$ProductNumber}_L_louroe.jpg";
	if (!file_exists ($base_dir . $image)) {
		$image = "/wp-content/themes/louroe/images/logo_blank.png";
	}
	$displayImage = $image;
	//$displayImage = "<img src='$image' width='200px;'>";
	
	$specifications = "products/specifications/{$ProductNumber}_louroe.pdf";
	$instructions = "products/instructions/{$ProductNumber}_louroe.pdf";
	
	$CategoryURL = str_replace(' ','-',$CategoryName);
	$Category = $CategoryName;
	if ($SubCategoryName != '' && $CategoryName != $SubCategoryName) { $Category .= "/{$SubCategoryName}"; }
	$URL = "www.louroe.com/products-systems/{$CategoryURL}/{$ProductSerial}";
	
	$Description = replace_products ( $Description_Web );
	
	$status = '';
	
	/*
	$status = '';
	$extension = substr($image, -3);
	if (copy($base_dir . $image, $base_dir . "cron/product-press-kit/products/{$ProductSerial}.{$extension}")) {
		$status = 'copied';
	}
	*/
	
	/*
	
	$status = '';
	$extension = 'pdf';
	if ( file_exists( $base_dir . $specifications ) ) {
		if (copy($base_dir . $specifications, $base_dir . "cron/product-press-kit/specifications/{$ProductSerial}.{$extension}")) {
			$status = 'copied';
		}
	}
	*/
	
	/*
	$status = '';
	$extension = 'pdf';
	if ( file_exists( $base_dir . $instructions ) ) {
		if (copy($base_dir . $instructions, $base_dir . "cron/product-press-kit/instructions/{$ProductSerial}.{$extension}")) {
			$status = 'copied';
		}
	}
	*/
	
	$output = false;
	if ($output) {	
		echo "<p class='list'>$ProductSerial: $ProductName ($ProductNumber) - {$status}</p>";
		echo "<p class='indent'>image: $displayImage</p>";
		echo "<p class='indent'>specifications: {$specifications}</p>";
		echo "<p class='indent'>instructions: {$instructions}</p>";
	}
	
	
	

	$objPHPExcel->getActiveSheet()->SetCellValue("A{$row}", $ProductSerial);
	$objPHPExcel->getActiveSheet()->SetCellValue("B{$row}", $ProductName);
	$objPHPExcel->getActiveSheet()->SetCellValue("C{$row}", $Category);
	$objPHPExcel->getActiveSheet()->SetCellValue("D{$row}", $URL);
	$objPHPExcel->getActiveSheet()->SetCellValue("E{$row}", $Description);
	$objPHPExcel->getActiveSheet()->SetCellValue("F{$row}", $Description_Short);
	$objPHPExcel->getActiveSheet()->SetCellValue("G{$row}", $Keywords);
	
	
}

$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 

// Save Excel 2007 file
echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save('product-press-kit/product-listing.xlsx');

// Echo done
echo date('H:i:s') . " Done writing file.\r\n";


?>
<style>
	body p { font-family: courier; font-size:12px; margin: 0 0 0 0; }
	.indent { padding-left:10px; }
	.list { margin: 10px 0 10px 0; color:blue; }
</style>