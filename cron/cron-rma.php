<? 
//if ( $_SERVER['REMOTE_ADDR'] != '108.23.17.241' ) { exit; }

// /usr/php/54/usr/bin/php /home1/louroeco/www/cron/cron-rma.php


error_reporting(E_ALL);
ini_set( 'display_errors','1'); 

require_once '../include/data_basic_inc.php'; 
require_once '../include/mail_inc.php';

$debug = (isset($_GET['debug'])) ? true : false;
//$debug = true;

function ucsentence($i_str, $i_lowercase = false) {  
  $i_lowercase && ($i_str = strtolower($i_str));  
  return preg_replace('/(^|[\.!?]"?\s+)([a-z])/e', '"$1" . ucfirst("$2")', $i_str);  
}  

// get rma information
$sql = " select r.* from rma r 
where left(r.RMAStatus,8) != 'Complete'
order by r.RMARequestDate asc, r.Company  ";
$mysqli_result = mysqli_query($mysqli, $sql); if (!$mysqli_result) { SQLError($sql); }

$row_number = 0;

$output = '';

$output .= "
	<style>
		
		body, td { font-family: Arial, Helvetica, sans-serif; font-size:12px; }
		table { border-collapse: collapse; border-spacing: 0; }
		td { padding: 10px; vertical-align:top; }
		td { border-top: 1px solid #eee; }
	
		a {text-decoration:none; }
	
		.model { font-family: 'Courier', Courier, monospace; }
		.model-info { color: #b0611d; }
		.internal-note { color:#67483d; }
	
		.col1 { width: 300px; }
		.col2 { width: 400px; }
		.col3 { width: 400px; }
		
		.alt1 { background-color:#ffffff; }
		.alt2 { background-color:#eee; }
		
		.status { color: #008367; }
		.status-retention { color: #9f373b; }
	</style>
	";

	$output .= "\n<table>";
	$output .= "\n<tr><td class='alt2'>RMA Information</td><td class='alt2'>Item Information</td><td class='alt2'>Notes</td></tr>";
	
	while ($row = mysqli_fetch_assoc($mysqli_result)) {
		
		$row_number++;
		$row_class = ($row_number % 2) ? 'alt1' : 'alt2';
		
		foreach ($row as $key => $value ) { $$key=''; if (isset($value)) { $$key = $value; } };
		
		$info = "";
		$info .= "<a href='https://www.louroe.com/rma/manager.php?wrmaid={$WRMAID}' target='_rma-manager'>WRMAID: $WRMAID</a>";
		$info .= "<br>RMAID: $RMAID";
		$info .= "<br>RMAKey: $RMAKey<br>";
		
		if ($Company != '') { $info .= "<br>$Company"; }
		if ($Address1 != '') { $info .= "<br>$Address1"; }
		if ($Address2 != '') { $info .= "<br>$Address2"; }
		if ($City != '' || $State != '' || $Zip != '') { $info .= "<br>$City, $State $Zip"; }
		if ($ContactName != '') { $info .= "<br><br>Contact: $ContactName"; }
		if ($Phone != '' || $Email !='' || $Fax !='' ) { 
			$Fax = ($Fax != '') ? "$Fax(Fax)" : "";
			$Email = ($Email != '') ? "<a href='mailto:$Email'>$Email</a>" : "";
			$Phone = ($Phone != '') ? "$Phone(Ph)" : "";
			$info .= "<br>$Phone $Email $Fax"; 
		}

		$date1 = new DateTime();
		$date2 = new DateTime($RMARequestDate);
		$interval = $date1->diff($date2);
		$RetentionDays = $interval->days;
		$RetentionClass = ($RetentionDays >= 10) ? 'status-retention' : '';
		
		$RMARequestDate = date( 'l, m-d-Y h:i A', strtotime($RMARequestDate));

		$info .= "<br><br>Request Date: $RMARequestDate";
		$info .= "<br>Retention: <span class='{$RetentionClass}'>$RetentionDays day"; if ($RetentionDays != 1) { $info .= "s"; } $info .= "</span>";
		$info .= "<br>Status: <span class='status'>$RMAStatus</span>";
		
		$item = '';
		
		$a=0;
		for ($i=1; $i<=5; $i++) {
			$Model = "Model{$i}"; $SerialNo = "SerialNo{$i}"; $Qty = "Qty{$i}";
			if ( $$Model != '') {
				$a++;
				$item .= "<span class='model'>$a. Model:<span class='model-info'>{$$Model}</span>&nbsp;&nbsp;SerialNo:<span class='model-info'>{$$SerialNo}</span>&nbsp;&nbsp;Qty:<span class='model-info'>{$$Qty}</span></span><br>";
			}
		}
		$item .= "<br>Repair And Return:$RepairAndReturn, Return For Credit:$ReturnForCredit";
		$Problem = ucsentence($Problem,true);
		$item .= "<br><br><b>Problem:</b><br>$Problem";
		
		
		// email history
		$history = "";
		$sql = " select * from rma_history where wrmaid = '{$WRMAID}' order by CreateDate desc ";
		$mysqli_result_history = mysqli_query($mysqli, $sql); if (!$mysqli_result_history) { SQLError($sql); }		
		while ($row_history = mysqli_fetch_assoc($mysqli_result_history)) {
			$CreateDate = date( 'l, m-d-Y h:i A', strtotime($row_history['CreateDate']));
			$Description = $row_history['Description'];
			$history .= "<br>$CreateDate, $Description";
		}
		$item .= "<br><br><b>Email History:</b>{$history}";
		
		// notes
		$notes = "";
		$sql = " select * from rma_details where wrmaid = '{$WRMAID}' order by CreateDate desc ";
		$mysqli_result_history = mysqli_query($mysqli, $sql); if (!$mysqli_result_history) { SQLError($sql); }		
		while ($row_history = mysqli_fetch_assoc($mysqli_result_history)) {
			$CreateDate = date( 'l, m-d-Y h:i A', strtotime($row_history['CreateDate']));
			$WebNotes = $row_history['WebNotes'];
			$InternalNotes = $row_history['InternalNotes'];
			$Author = $row_history['Author'];
			
			$notes .= "<b><i>$CreateDate by $Author</i></b>";
			if ($WebNotes != '') { $notes .= "<br><br><b>Customer:</b> $WebNotes"; }
			if ($InternalNotes != '') { $notes .= "<br><br><span class='internal-note'><b>Internal:</b> $InternalNotes</span>"; }
			$notes .= "<br><br>";
		}
		
		
		// final output
		$output .= "\n<tr>";
		$output .= "<td class='col1 $row_class'>{$info}</td><td class='col2 $row_class'>{$item}</td><td class='col3 $row_class'>{$notes}</td>";
		$output .= "\n</tr>";

}
$output .= "\n</table>";


if ($debug) { echo $output; } 
	else {
	$mail = new mailWrapper();
	//$email = 'rma@louroe.com';
	$to = 'rma@louroe.com';
	$cc = 'pilar@louroe.com; Sara@louroe.com; Cameron@louroe.com; Salvador@louroe.com; Stephen@louroe.com; Mathilde@louroe.com;';
	$bcc='';

	$ReportDate = date( 'l, m-d-Y' );
	$mail->subject = "Weekly RMA Summary: $ReportDate";

	$message = $output;
	$mail->body = $message;
	$mail->to = $to;
	$mail->bcc = $bcc;
	$mail->cc = $cc;
	$mail->from = "info@louroe.com";
	$mail->fromName = 'Louroe RMA Info';
	$mail->send();
	
	echo 'Done';
	}
?>
